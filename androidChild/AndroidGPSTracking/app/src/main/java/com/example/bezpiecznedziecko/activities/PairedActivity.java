package com.example.bezpiecznedziecko.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.bezpiecznedziecko.R;
import com.example.bezpiecznedziecko.global.CommonFields;

public class PairedActivity extends Activity {

    private TextView labelChildName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paired);

        final int NOTIFICATION_ID = 1234;
        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNM.cancel(NOTIFICATION_ID);
        CommonFields.setConfirmationShown(false);

        labelChildName = (TextView) findViewById(R.id.labelChildName);
        labelChildName.setText(CommonFields.getChildName());
    }
}
