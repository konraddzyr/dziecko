package com.example.bezpiecznedziecko.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.bezpiecznedziecko.services.BackgroundService;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent service) {
        context.startService(new Intent(context, BackgroundService.class));
    }

}
