package com.example.bezpiecznedziecko.global;

public class CommonFields {

    public static final String APP_URI = "http://87.246.222.167:8080/dziecko";
    public static final int IN_AREA_INTERVAL = 30000; //ms
    public static final int OUT_OF_AREA_INTERVAL = 5000; //ms

    private static String token = "";
    private static String childName = "";
    private static int interval = IN_AREA_INTERVAL;
    private static boolean outOfArea = false;
    private static boolean confirmationShown = false;

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        CommonFields.token = token;
    }

    public static String getChildName() {
        return childName;
    }

    public static void setChildName(String childName) {
        CommonFields.childName = childName;
    }

    public static int getInterval() {
        return interval;
    }

    public static boolean isOutOfArea() {
        return outOfArea;
    }

    public static void setOutOfArea(boolean isOutOfArea) {
        CommonFields.outOfArea = isOutOfArea;
    }

    public static void setOutOfAreaInterval() {
        interval = OUT_OF_AREA_INTERVAL;
    }

    public static void setInAreaInterval() {
        interval = IN_AREA_INTERVAL;
    }

    public static boolean isConfirmationShown() {
        return confirmationShown;
    }

    public static void setConfirmationShown(boolean confirmationShown) {
        CommonFields.confirmationShown = confirmationShown;
    }
}
