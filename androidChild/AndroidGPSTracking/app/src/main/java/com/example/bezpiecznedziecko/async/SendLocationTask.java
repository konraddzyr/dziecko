package com.example.bezpiecznedziecko.async;

import android.os.AsyncTask;

import com.example.bezpiecznedziecko.activities.MainActivity;
import com.example.bezpiecznedziecko.R;
import com.example.bezpiecznedziecko.global.CommonFields;
import com.example.bezpiecznedziecko.global.GPSTracker;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Send location to server.
 */
public class SendLocationTask extends AsyncTask {
    public AsyncResponse delegate = null;

    public Object doInBackground(Object[] args) {
        GPSTracker gps = MainActivity.getGps();
        if (gps.canGetLocation()) {
            if (!CommonFields.getToken().isEmpty()) {
                String uri = CommonFields.APP_URI + "/children/validate?token=" + CommonFields.getToken() +
                        "&latitude=" + String.valueOf(gps.getLatitude()) + "&longitude=" + String.valueOf(gps.getLongitude());

                AsyncHttpClient client = new AsyncHttpClient();
                client.get(uri, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getBoolean("outOfAreas") && !CommonFields.isOutOfArea()) {
                                CommonFields.setOutOfAreaInterval();
                            } else {
                                CommonFields.setInAreaInterval();
                                CommonFields.setOutOfArea(false);
                            }
                            if(obj.getBoolean("confirmationRequired") && !CommonFields.isConfirmationShown()) {
                                CommonFields.setConfirmationShown(true);
                                delegate.processFinishWithAlert();
                            }
                        } catch (JSONException e) {
                            delegate.processFinishWithToast(R.string.jsonError);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable response) {
                        delegate.processFinishWithToast(R.string.validateError);
                    }
                });
            }

        } else {
            delegate.showGpsSettings();
        }
        delegate.instantiateNewLocationTask();
        return this;
    }

}

