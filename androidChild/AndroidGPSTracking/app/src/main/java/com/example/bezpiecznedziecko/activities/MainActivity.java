package com.example.bezpiecznedziecko.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bezpiecznedziecko.R;
import com.example.bezpiecznedziecko.async.AsyncResponse;
import com.example.bezpiecznedziecko.async.SendLocationTask;
import com.example.bezpiecznedziecko.global.CommonFields;
import com.example.bezpiecznedziecko.global.GPSTracker;
import com.example.bezpiecznedziecko.receivers.AlarmReceiver;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements AsyncResponse {

    private PendingIntent pendingIntent;
    private AlarmManager manager;

    private Button btnStart;
    private EditText login;
    private EditText passwd;
    private EditText childName;

    private static GPSTracker gps;
    private static SendLocationTask locationTask = new SendLocationTask();

    public static GPSTracker getGps() {
        return gps;
    }

    public static SendLocationTask getLocationTask() {
        return locationTask;
    }

    @Override
    public void instantiateNewLocationTask() {
        locationTask = new SendLocationTask();
        locationTask.delegate = this;
    }

    @Override
    public void processFinishWithToast(final int response) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void processFinishWithAlert() {
        notifyConfirmationRequirement();
    }

    @Override
    public void showGpsSettings() {
        this.runOnUiThread(new Runnable() {
            public void run() {
                gps.showSettingsAlert();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        gps = new GPSTracker(MainActivity.this);
        locationTask.delegate = this;

        btnStart = (Button) findViewById(R.id.buttonStart);
        login = (EditText) findViewById(R.id.inputLogin);
        passwd = (EditText) findViewById(R.id.inputPasswd);
        childName = (EditText) findViewById(R.id.inputChildName);

        btnStart.setOnClickListener(handlePairButtonAction());

        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

        this.startAlarm();
    }

    /**
     * Starts alarm manager, which calls location service periodically.
     */
    private void startAlarm() {
        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), CommonFields.getInterval(), pendingIntent);
    }

    private View.OnClickListener handlePairButtonAction() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (login.getText().toString().isEmpty() || passwd.getText().toString().isEmpty() ||
                        childName.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.fillInputs, Toast.LENGTH_LONG).show();
                    return;
                }

                AsyncHttpClient client = new AsyncHttpClient();
                String md5Hex = new String(Hex.encodeHex(DigestUtils.md5(passwd.getText().toString())));
                String uri = CommonFields.APP_URI + "/parents/pair?parentName=" + login.getText().toString().trim()
                        + "&password=" + md5Hex + "&childName=" + childName.getText().toString().trim();
                client.get(uri, handlePairing());
            }
        };
    }

    /**
     * Handles pairing REST operation.
     *
     * @return AsyncHttpResponseHandler
     */
    private AsyncHttpResponseHandler handlePairing() {
        return new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    CommonFields.setToken(obj.getString("token"));
                    if (obj.getBoolean("newRegistered")) {
                        Toast.makeText(getApplicationContext(), R.string.newRegistered, Toast.LENGTH_LONG).show();
                    }
                    CommonFields.setChildName(childName.getText().toString().trim());

                    Intent pairedActivityIntent = new Intent(MainActivity.this, PairedActivity.class);
                    startActivity(pairedActivityIntent);
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), R.string.jsonError, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable response) {
                Toast.makeText(getApplicationContext(), R.string.toastError, Toast.LENGTH_LONG).show();
            }
        };
    }

    /**
     * Creates and notifies confirmation requirement notification.
     */
    private void notifyConfirmationRequirement() {
        final int NOTIFICATION_ID = 1234;
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.abc_ic_search)
                        .setContentTitle(getResources().getString(R.string.notificationTitle))
                        .setContentText(getResources().getString(R.string.notificationText));

        Intent targetIntent = new Intent(this, ConfirmationActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, targetIntent, PendingIntent.FLAG_ONE_SHOT);
        builder.setContentIntent(contentIntent);
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = builder.build();

        n.defaults |= Notification.DEFAULT_ALL;

        nManager.notify(NOTIFICATION_ID, n);
    }
}