package com.example.bezpiecznedziecko.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.bezpiecznedziecko.activities.MainActivity;

/**
 * BackgroundService calls periodically a task, which sends location to server
 */
public class BackgroundService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void onStart(Intent intent, int startId) {
        MainActivity.getLocationTask().execute();
    }

}
