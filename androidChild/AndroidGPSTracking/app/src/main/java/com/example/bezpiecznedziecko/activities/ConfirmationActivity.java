package com.example.bezpiecznedziecko.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.bezpiecznedziecko.R;
import com.example.bezpiecznedziecko.global.CommonFields;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class ConfirmationActivity extends Activity {

    private Button buttonOk;
    private Button buttonHelp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmation);

        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        final int NOTIFICATION_ID = 1234;
        mNM.cancel(NOTIFICATION_ID);

        buttonOk = (Button) findViewById(R.id.buttonOK);
        buttonHelp = (Button) findViewById(R.id.buttonHelp);

        buttonHelp.setOnClickListener(handleButtons(true));
        buttonOk.setOnClickListener(handleButtons(false));
    }


    /**
     * Handles confirmation buttons click action.
     *
     * @param value response value (true - problem, false - ok)
     * @return
     */
    private View.OnClickListener handleButtons(final boolean value) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AsyncHttpClient client = new AsyncHttpClient();
                String uri = CommonFields.APP_URI + "/children/" + CommonFields.getToken() + "/confirmation?value=" + value;
                client.get(uri, handleResponse());
            }
        };
    }

    /**
     * Creates and starts MainActivity intent.
     */
    private void startPairedActivity() {
        Intent pairedActivityIntent = new Intent(this, PairedActivity.class);
        startActivity(pairedActivityIntent);
    }

    /**
     * Handles confirmation REST action.
     *
     * @return rest response handler
     */
    private AsyncHttpResponseHandler handleResponse() {
        return new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                startPairedActivity();
            }

            @Override
            public void onFailure(Throwable response) {
                Toast.makeText(ConfirmationActivity.this, R.string.validateError, Toast.LENGTH_LONG).show();
            }
        };
    }
}
