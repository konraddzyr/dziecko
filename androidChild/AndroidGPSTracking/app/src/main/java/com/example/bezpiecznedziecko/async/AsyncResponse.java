package com.example.bezpiecznedziecko.async;

public interface AsyncResponse {

    /**
     * Creates and shows toast.
     *
     * @param output text to show
     */
    void processFinishWithToast(int output);

    /**
     * Notifies confirmation requirement.
     */
    void processFinishWithAlert();

    /**
     * Shows modal for GPS settings.
     */
    void showGpsSettings();

    /**
     * Instantiates new task for alarm manager.
     */
    void instantiateNewLocationTask();

}
