package pl.pollub.rodzicbezpiecznegodziecka.globals;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pl.pollub.rodzicbezpiecznegodziecka.R;

public class Helper {

    /**
     * Converts JSON from string response to list of objects.
     *
     * @param response JSON object
     * @param context application context
     * @return list of object
     */
    public static List<ValidatedChild> jsonToList(String response, Context context) {
        List<ValidatedChild> result = new ArrayList<ValidatedChild>();

        try {
            JSONArray ja = new JSONArray(response);
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jObject = ja.getJSONObject(i);
                ValidatedChild element = new ValidatedChild(jObject.getString("name"), jObject.getBoolean("alarm"), jObject.getBoolean("childResponsed"), jObject.getBoolean("response"));
                result.add(element);
            }
        } catch (JSONException e) {
            Toast.makeText(context, R.string.jsonError, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        return result;
    }
}
