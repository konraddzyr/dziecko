package pl.pollub.rodzicbezpiecznegodziecka.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import pl.pollub.rodzicbezpiecznegodziecka.R;
import pl.pollub.rodzicbezpiecznegodziecka.globals.CommonFields;
import pl.pollub.rodzicbezpiecznegodziecka.receivers.AlarmReceiver;

public class MainActivity extends Activity {

    /* Interval for alarm manager. */
    private static final int INTERVAL = 5000; // ms

    private PendingIntent pendingIntent;
    private AlarmManager manager;

    private Button btnLogin;
    private EditText login;
    private EditText passwd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnLogin = (Button) findViewById(R.id.buttonStart);
        login = (EditText) findViewById(R.id.inputLogin);
        passwd = (EditText) findViewById(R.id.inputPasswd);

        btnLogin.setOnClickListener(handleLoginClick());

        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

        this.startAlarm();
    }

    @Override
    public void onResume() {
        super.onResume();
        login.setText("");
        passwd.setText("");
        CommonFields.resetParentData();
    }

    /**
     * Starts alarm.
     */
    private void startAlarm() {
        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), INTERVAL, pendingIntent);
    }

    /**
     * Handles login button click action.
     *
     * @return on click listener
     */
    private View.OnClickListener handleLoginClick() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (login.getText().toString().isEmpty() || passwd.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.fillInputs, Toast.LENGTH_LONG).show();
                    return;
                }

                AsyncHttpClient client = new AsyncHttpClient();
                String md5Hex = new String(Hex.encodeHex(DigestUtils.md5(passwd.getText().toString())));
                String uri = CommonFields.APP_URI + "/authentication/login/mobile?name=" +
                        login.getText() + "&password=" + md5Hex;
                client.get(uri, handleLogin());
            }
        };
    }

    /**
     * Handles login REST action.
     *
     * @return response handler
     */
    private AsyncHttpResponseHandler handleLogin() {
        return new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                int id = Integer.parseInt(response);
                if (-1 == id) {
                    Toast.makeText(MainActivity.this, R.string.badId, Toast.LENGTH_LONG).show();
                    return;
                }
                CommonFields.setParentId(id);
                CommonFields.setUserName(login.getText().toString());

                Intent loggedActivityIntent = new Intent(MainActivity.this, LoggedActivity.class);
                startActivity(loggedActivityIntent);
            }

            @Override
            public void onFailure(Throwable response) {
                Toast.makeText(MainActivity.this, R.string.toastError, Toast.LENGTH_LONG).show();
            }
        };
    }

}