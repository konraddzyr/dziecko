package pl.pollub.rodzicbezpiecznegodziecka.globals;

public class CommonFields {

    public static final String APP_URI = "http://87.246.222.167:8080/dziecko";

    private static int parentId = -1;
    private static String userName = "";
    private static boolean outOfAreasShown = false;

    public static int getParentId() {
        return parentId;
    }

    public static void setParentId(int parentId) {
        CommonFields.parentId = parentId;
    }

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        CommonFields.userName = userName;
    }

    public static boolean isOutOfAreasShown() {
        return outOfAreasShown;
    }

    public static void setOutOfAreasShown(boolean outOfAreasShown) {
        CommonFields.outOfAreasShown = outOfAreasShown;
    }

    /**
     * Resets parent's data for logout and stop validating children.
     */
    public static void resetParentData() {
        parentId = -1;
        userName = "";
    }
}
