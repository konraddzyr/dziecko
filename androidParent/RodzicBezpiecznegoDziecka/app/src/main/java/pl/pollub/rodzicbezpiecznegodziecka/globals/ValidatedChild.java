package pl.pollub.rodzicbezpiecznegodziecka.globals;

public class ValidatedChild {
    private String name;
    private Boolean alarm;
    private Boolean childRespond;
    private Boolean response;

    public ValidatedChild(String name, Boolean alarm, Boolean childRespond, Boolean response) {
        this.name = name;
        this.alarm = alarm;
        this.childRespond = childRespond;
        this.response = response;
    }

    public String getName() {
        return name;
    }

    public Boolean getAlarm() {
        return alarm;
    }

    public Boolean getChildRespond() {
        return childRespond;
    }

    public Boolean getResponse() {
        return response;
    }
}
