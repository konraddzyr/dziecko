package pl.pollub.rodzicbezpiecznegodziecka.activities;


import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import pl.pollub.rodzicbezpiecznegodziecka.R;
import pl.pollub.rodzicbezpiecznegodziecka.globals.CommonFields;

public class LoggedActivity extends Activity {
    private Button btnLogout;
    private TextView labelParentName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logged);

        final int NOTIFICATION_ID = 123;
        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNM.cancel(NOTIFICATION_ID);

        btnLogout = (Button) findViewById(R.id.buttonLogout);
        labelParentName = (TextView) findViewById(R.id.labelParentName);

        labelParentName.setText(CommonFields.getUserName());

        btnLogout.setOnClickListener(handleClick());
    }

    /**
     * Handles logout button click.
     *
     * @return on click listener
     */
    private View.OnClickListener handleClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(LoggedActivity.this, MainActivity.class);
                CommonFields.resetParentData();
                startActivity(main);
            }
        };
    }
}
