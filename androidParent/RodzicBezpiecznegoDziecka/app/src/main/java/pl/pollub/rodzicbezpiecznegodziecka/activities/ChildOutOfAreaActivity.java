package pl.pollub.rodzicbezpiecznegodziecka.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import pl.pollub.rodzicbezpiecznegodziecka.R;
import pl.pollub.rodzicbezpiecznegodziecka.globals.CommonFields;

/**
 * Created by Konrad on 2015-06-07.
 */
public class ChildOutOfAreaActivity extends Activity {

    private TextView labelChildOutOfAreas;
    private Button buttonRequestConfirmation;
    private Button buttonBack;
    private String childName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child_out_of_area);

        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        final int NOTIFICATION_ID = 1234;
        mNM.cancel(NOTIFICATION_ID);

        childName = getIntent().getExtras().getString("name");
        labelChildOutOfAreas = (TextView) findViewById(R.id.labelChildOutOfArea);
        buttonRequestConfirmation = (Button) findViewById(R.id.buttonContact);
        buttonBack = (Button) findViewById(R.id.buttonBack);

        labelChildOutOfAreas.setText(childName + " jest poza wyznaczonym obszarem");

        buttonRequestConfirmation.setOnClickListener(handleClick());
        buttonBack.setOnClickListener(handleBack());
    }

    private View.OnClickListener handleClick() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AsyncHttpClient client = new AsyncHttpClient();
                String uri = CommonFields.APP_URI + "/parents/" + CommonFields.getParentId() + "/children/" +
                        childName + "/requestConfirmation";
                client.get(uri, handleConfirmationRequest());
            }
        };
    }

    private AsyncHttpResponseHandler handleConfirmationRequest() {
        return new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                CommonFields.setOutOfAreasShown(false);
                Intent intent = new Intent(ChildOutOfAreaActivity.this, LoggedActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Throwable response) {
                Toast.makeText(getApplicationContext(), R.string.serverError, Toast.LENGTH_LONG).show();
            }
        };
    }

    private View.OnClickListener handleBack() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent backToLoggedIntent = new Intent(ChildOutOfAreaActivity.this, LoggedActivity.class);
                startActivity(backToLoggedIntent);
            }
        };
    }
}
