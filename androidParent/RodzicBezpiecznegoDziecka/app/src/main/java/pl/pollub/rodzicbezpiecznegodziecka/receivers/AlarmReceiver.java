package pl.pollub.rodzicbezpiecznegodziecka.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import pl.pollub.rodzicbezpiecznegodziecka.services.BackgroundService;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent service) {
        context.startService(new Intent(context, BackgroundService.class));
    }

}
