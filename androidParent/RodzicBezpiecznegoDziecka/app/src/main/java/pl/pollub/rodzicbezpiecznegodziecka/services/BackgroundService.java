package pl.pollub.rodzicbezpiecznegodziecka.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.List;

import pl.pollub.rodzicbezpiecznegodziecka.R;
import pl.pollub.rodzicbezpiecznegodziecka.activities.ChildOutOfAreaActivity;
import pl.pollub.rodzicbezpiecznegodziecka.activities.LoggedActivity;
import pl.pollub.rodzicbezpiecznegodziecka.globals.CommonFields;
import pl.pollub.rodzicbezpiecznegodziecka.globals.Helper;
import pl.pollub.rodzicbezpiecznegodziecka.globals.ValidatedChild;

public class BackgroundService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void onStart(Intent intent, int startId) {
        if (-1 == CommonFields.getParentId()) {
            return;
        }
        AsyncHttpClient client = new AsyncHttpClient();
        String uri = CommonFields.APP_URI + "/parents/" + CommonFields.getParentId() + "/children/validate";
        client.get(uri, handleResponse());
    }

    /**
     * Handles children validation REST.
     *
     * @return response handler
     */
    private AsyncHttpResponseHandler handleResponse() {
        return new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String response) {
                List<ValidatedChild> list = Helper.jsonToList(response, BackgroundService.this);
                for (ValidatedChild ch : list) {
                    if (ch.getAlarm() && !CommonFields.isOutOfAreasShown()) {
                        BackgroundService.this.notifyOutOfArea(ch.getName());
                    }
                    if (ch.getChildRespond()) {
                        BackgroundService.this.notifyResponse(ch.getName(), ch.getResponse());
                    }
                }
            }

            @Override
            public void onFailure(Throwable response) {
                Toast.makeText(BackgroundService.this, R.string.serverError, Toast.LENGTH_LONG).show();
            }
        };
    }

    /**
     * Creates and notifies child out of area notification.
     *
     * @param name child name
     */
    private void notifyOutOfArea(String name) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.abc_ic_search)
                        .setContentTitle(name + getString(R.string.notificationTitleChildOutOfArea))
                        .setContentText(getString(R.string.notificationTextChildOutOfArea));
        final int NOTIFICATION_ID = 1234;

        Intent targetIntent = new Intent(this, ChildOutOfAreaActivity.class);
        targetIntent.putExtra("name", name);
        PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, targetIntent, PendingIntent.FLAG_ONE_SHOT);
        builder.setContentIntent(contentIntent);
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = builder.build();

        n.defaults |= Notification.DEFAULT_ALL;

        CommonFields.setOutOfAreasShown(true);
        nManager.notify(NOTIFICATION_ID, n);
    }

    /**
     * Creates and notifies child response notification.
     *
     * @param name     child name
     * @param response child response (true - trouble, false - ok)
     */
    private void notifyResponse(String name, Boolean response) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.abc_ic_search)
                        .setContentTitle(name + " " + getString(R.string.responsed))
                        .setContentText((response ? getString(R.string.help) : getString(R.string.ok)));
        final int NOTIFICATION_ID = 123;

        Intent targetIntent = new Intent(this, LoggedActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, targetIntent, PendingIntent.FLAG_ONE_SHOT);
        builder.setContentIntent(contentIntent);
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = builder.build();

        n.defaults |= Notification.DEFAULT_ALL;

        nManager.notify(NOTIFICATION_ID, n);
    }
}
