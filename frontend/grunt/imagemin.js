module.exports = function(grunt, options) {
  return {
    dist: {
      files: [
        {
          expand: true,
          cwd: "<%= globalConfig.src %>/img",
          src: "**/*.{png,jpg,jpeg}",
          dest: "<%= globalConfig.dist %>/img"
        }
      ]
    }
  }
};
