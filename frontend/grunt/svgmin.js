module.exports = function(grunt, options) {
  return {
    dist: {
      options: { // Configuration that will be passed directly to SVGO
        plugins: [
          { convertPathData: false }
        ]
      },
      files: [
        {
          expand: true,
          cwd: "<%= globalConfig.src %>/img",
          src: "**/*.svg",
          dest: "<%= globalConfig.dist %>/img"
        }
      ]
    }
  }
};
