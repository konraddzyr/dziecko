module.exports = function(grunt, options) {
  return {
    index: {
      files: [
        {
          src: "<%= globalConfig.src %>/index.html",
          dest: "<%= globalConfig.tmp %>/index.html"
        }
      ]
    }
  }
};
