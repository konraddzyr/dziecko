module.exports = function(grunt, options) {
  return {
    dist: {
      files: [
        {
          expand: true,
          cwd: "<%= globalConfig.tmp %>/js/",
          src: "*.js",
          dest: "<%= globalConfig.tmp %>/js/"
        }
      ]
    }
  }
};
