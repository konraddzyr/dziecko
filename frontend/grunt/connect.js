module.exports = function(grunt, options) {

  var middlewareWithProxies = function(connect, options) {
    if (!Array.isArray(options.base)) {
      options.base = [options.base];
    }

    // Setup the proxy
    var middlewares = [require("grunt-connect-proxy/lib/utils").proxyRequest];

    // Serve static files.
    options.base.forEach(function(base) {
      middlewares.push(connect.static(base));
    });

    // Make directory browse-able.
    var directory = options.directory || options.base[options.base.length - 1];
    middlewares.push(connect.directory(directory));

    return middlewares;
  };

  return {
    options: {
      port: 8888,
      // Change this to "localhost" to disable access from outside.
      hostname: "0.0.0.0"
    },
    proxies: [
      {
        context: "/api",
        host: "localhost",
        port: options.globalConfig.backendPort,
        rewrite: {
          "^/api/": "/dziecko/"
        }
      }
    ],
    livereload: {
      options: {
        base: ["<%= globalConfig.tmp %>", "<%= globalConfig.src %>", "<%= globalConfig.dist %>"],
        middleware: middlewareWithProxies,
        livereload: options.globalConfig.livereloadPort,
        open: "http://localhost:<%= connect.options.port %>"
      }
    },
    dist: {
      options: {
        base: ["<%= globalConfig.dist %>"],
        middleware: middlewareWithProxies,
        open: "http://localhost:<%= connect.options.port %>"
      }
    }
  }
};
