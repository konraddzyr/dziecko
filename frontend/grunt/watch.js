module.exports = function(grunt, options) {
  return {
    javascript: {
      files: ["<%= globalConfig.src %>/js/**/*.js"],
      tasks: ["copy:js"]
    },
    styles: {
      files: ["<%= globalConfig.src %>/css/**/*.css"],
      tasks: ["copy:styles", "autoprefixer"]
    },
    index: {
      files: ["<%= globalConfig.src %>/index.html"],
      tasks: ["env:dev", "preprocess"]
    },
    livereload: {
      options: {
        livereload: options.globalConfig.livereloadPort
      },
      files: [
        "<%= globalConfig.tmp %>/index.html",
        "<%= globalConfig.src %>/html/**/*.html",
        "<%= globalConfig.tmp %>/css/**/*.css",
        "<%= globalConfig.tmp %>/js/**/*.js",
        "<%= globalConfig.src %>/img/{,**/}*.{png,jpg,jpeg,gif,webp,svg}"
      ]
    }
  }
};
