module.exports = function(grunt, options) {
  return {
    options: {
      dest: "<%= globalConfig.dist %>"
    },
    html: ["<%= globalConfig.tmp %>/index.html"]
  }
};
