module.exports = function(grunt, options) {
  return {
    dist: {
      files: [
        {
          dot: true,
          src: [
            "<%= globalConfig.tmp %>",
            "<%= globalConfig.dist %>/*"
          ]
        }
      ]
    },
    server: "<%= globalConfig.tmp %>"
  }
};
