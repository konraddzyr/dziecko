module.exports = function(grunt, options) {
  return {
    options: ["last 1 version"],
    dist: {
      files: [
        {
          expand: true,
          cwd: "<%= globalConfig.tmp %>/css/",
          src: "**/*.css",
          dest: "<%= globalConfig.tmp %>/css/"
        }
      ]
    }
  }
};
