/**
 * Created by dlw on 2014-08-26.
 */
module.exports = function(grunt, options) {
  return {
    options: {
      // See: http://compass-style.org/help/tutorials/configuration-reference/
      sassDir: "<%= globalConfig.src %>", // Directory with scss/sass files
      cssDir: "<%= globalConfig.tmp %>", // Destination directory for compiled css files

      generatedImagesDir: "<%= globalConfig.tmp %>/assets/images/generated", // Destination directory for generated images
      httpGeneratedImagesPath: "/assets/images/generated", // Http path for generated images. Use generated-image-url()
      imagesDir: "<%= globalConfig.src %>/assets/images", // Directory with images
      httpImagesPath: "/assets/images", // Http path for images. Use image-url()

      javascriptsDir: "<%= globalConfig.src %>/app", // JavaScript files directory

      fontsDir: "<%= globalConfig.src %>/assets/fonts", // Fonts directory
      httpFontsPath: "/assets/fonts", // Http fonts directory. Use font-url()

      importPath: ["<%= globalConfig.src %>/bower_components", "<%= globalConfig.src %>/styles", "<%= globalConfig.src %>/app"],
      relativeAssets: false
    },
    dist: {
      // default options
    },
    server: {
      options: {
        debugInfo: false
      }
    }
  }
};
