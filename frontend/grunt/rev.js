module.exports = function(grunt, options) {
  return {
    dist: {
      files: {
        src: [
          "<%= globalConfig.dist %>/js/**/*.js",
          "<%= globalConfig.dist %>/css/*.css"
        ]
      }
    }
  }
};
