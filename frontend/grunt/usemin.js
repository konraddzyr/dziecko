module.exports = function(grunt, options) {
  return {
    options: {
      dirs: ["<%= globalConfig.dist %>"]
    },
    html: ["<%= globalConfig.dist %>/html/**/*.html", "<%= globalConfig.dist %>/index.html"],
    css: ["<%= globalConfig.dist %>/css/**/*.css"]
  }
};
