module.exports = function(grunt, options) {
  return {
    server: ["copy:styles", "preprocess"],
    test: ["copy:styles"],
    dist: {
      tasks: ["copy:styles", "imagemin", "svgmin", "htmlmin"],
      options: {
        logConcurrentOutput: true,
        limit: 6
      }}
  }
};
