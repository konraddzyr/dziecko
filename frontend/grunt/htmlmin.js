module.exports = function(grunt, options) {
  return {
    dist: {
      files: [
        {
          expand: true,
          cwd: "<%= globalConfig.tmp %>",
          src: ["index.html", "html/**/*.html"],
          dest: "<%= globalConfig.dist %>"
        }
      ]
    }
  }
};
