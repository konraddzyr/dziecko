module.exports = function(grunt, options) {
  return {
    "default": ["cleaning", "build"],
    "build": [
      "env:prod",
      "clean:dist",
      "copy:htmls",
      "preprocess",
      "useminPrepare",
      "concurrent:dist",
      "ngAnnotate",
      "autoprefixer",
      "concat",
      "copy:dist",
      "uglify",
      "rev",
      "usemin"
    ],
    "cleaning": [
      "clean:server",
      "concurrent:server",
      "autoprefixer"
    ],
    "server": [
      "env:dev",
      "clean:server",
      "concurrent:server",
      "autoprefixer",
      "configureProxies",
      "connect:livereload",
      "watch"
    ],
    "server:dist": ["build", "configureProxies", "connect:dist:keepalive"]
  };
};
