/**
 * Created by dlw on 2014-08-26.
 */
module.exports = function(grunt, options) {
  return {
    unit: {
      configFile: 'karma.conf.js',
      singleRun: true
    },
    continuous: {
      configFile: 'karma-e2e.conf.js'
    },
    unitDev: {
      configFile: 'karma.conf.js',
      singleRun: false,
      autoWatch: true
    }
  }
};
