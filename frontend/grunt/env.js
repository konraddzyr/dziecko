module.exports = function(grunt, options) {
  return {
    dev: {
      NODE_ENV: "DEVELOPMENT"
    },
    prod: {
      NODE_ENV: "PRODUCTION"
    }
  }
};
