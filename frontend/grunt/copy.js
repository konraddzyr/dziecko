// TODO simplify!
module.exports = function(grunt, options) {
  return {
    dist: {
      files: [
        {
          expand: true,
          dot: true,
          cwd: "<%= globalConfig.src %>",
          dest: "<%= globalConfig.dist %>",
          src: [
            "*.{ico,png,txt}",
            ".htaccess",
            "img/**/*.{gif,webp}"
          ]
        }
      ]
    },
    htmls: {
      files: [
        {
          expand: true,
          cwd: "<%= globalConfig.src %>",
          dest: "<%= globalConfig.tmp %>/",
          src: [
            "html/**/*.html"
          ]
        }
      ]
    },
    styles: {
      files: [
        {
          expand: true,
          cwd: "<%= globalConfig.src %>",
          dest: "<%= globalConfig.tmp %>/",
          src: ["css/**/*.css"]
        }
      ]
    },
    js: {
      expand: true,
      cwd: "<%= globalConfig.src %>",
      dest: "<%= globalConfig.dist %>/",
      src: ["/js/**/*.js"]
    }
  }
};
