/**
 * Created by dlw on 2014-08-26.
 */
module.exports = function(grunt, options) {
  return {
    dist: {
      html: ["<%= globalConfig.dist %>/*.html"]
    }
  }
};
