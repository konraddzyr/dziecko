module.exports = function(grunt) {
  // global configuration
  var globalConfig = {
    src: "src",
    dist: "dist",
    tmp: ".tmp",
    livereloadPort: 35730,
    backendPort: 8080
  };

  // measures the time each task takes
  require("time-grunt")(grunt);

  // load grunt config
  require("load-grunt-config")(grunt, {
    config: {
      globalConfig: globalConfig
    }
  });

};
