Array.prototype.remove = function (from, to) {
  var rest = this.slice(parseInt(to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

var module = angular.module("DzieckoModule", ["ngRoute", "uiGmapgoogle-maps", "restangular", "ngStorage", 'oitozero.ngSweetAlert', 'ui.utils']);

module.controller("MainCtrl", ["$scope", "$rootScope", "Restangular", "$location", "$sessionStorage", function ($scope, $rootScope, Restangular, $location, $sessionStorage) {
  $rootScope.$sessionStorage = $rootScope.$sessionStorage || $sessionStorage;

  $scope.logout = function () {

    Restangular.all("api/authentication/logout").customDELETE($rootScope.$sessionStorage.user.name).then(function () {
      $location.path("/");
      $rootScope.$sessionStorage.$reset();
    });
  }
}]);

module.config(["$routeProvider", function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "html/news.html",
      controller: "NewsCtrl"
    })
    .when("/registration", {
      templateUrl: "html/registration.html",
      controller: "RegistrationCtrl"
    })
    .when("/login", {
      templateUrl: "html/login.html",
      controller: "LoginCtrl"
    })
    //?x=:x&y=:y&childId=:childId
    .when("/map", {
      templateUrl: "html/map.html",
      controller: "MapCtrl"
    })
    .when("/parents/:parentId/children", {
      templateUrl: "html/children.html",
      controller: "ChildrenCtrl"
    })
    .when("/simulator", {
      templateUrl: "html/simulator.html",
      controller: "SimulatorCtrl"
    })
    .otherwise({
      redirectTo: "/"
    });
}]);

module.controller("NewsCtrl", ["$scope", "$rootScope", "$route", 'Restangular', function ($scope, $rootScope, $route, Restangular) {
  /*Restangular.all('api/news/').getList().then(function (news) {
   $scope.news = news;
   });*/
}]);

module.controller("SimulatorCtrl", ["$scope", "$rootScope", "$interval", '$http', function ($scope, $rootScope, $interval, $http) {
  var x = 22.5207;
  var y = 51.2671;

  var counter = 0;
  $scope.coords = (counter + ": " + x + ", " + y);

  var simulate = function () {
    if (counter % 2) {
      x += 0.0005;
    } else {
      y += 0.0005;
    }

    var req = {
      method: 'GET',
      url: "api/children/validate?token=tokenJacek&latitude=" + y + "&longitude=" + x,
      headers: {
        'Content-Length': 0
      },
      data: {}
    };
    $http(req).success(function (response) {

      /*if ("true" === response) {
       if (changePromise) {
       changePromise();
       changePromise = undefined;
       }
       }*/
    });

    counter++;

    $scope.coords = (counter + ": " + x + ", " + y);
    console.log($scope.coords);
  };

  var promise = $interval(simulate, 30000);

  var changePromise = function () {
    $interval.cancel(promise);
    promise = $interval(simulate, 5000, false);
  }
}]);

module.controller("ChildrenCtrl", ["$scope", "$rootScope", "$route", 'Restangular', function ($scope, $rootScope, $route, Restangular) {
  $scope.startEdition = function (kid) {
    $scope.tempKid = angular.copy(kid);
    kid.edition = true;
  };
  $scope.editionSave = function (kid) {
    Restangular.all("api/parents/" + $rootScope.$sessionStorage.user.id + "/children").customPOST($scope.tempKid).then(function (response) {
      kid = $scope.tempKid;
      kid.edition = false;

      // TODO: to z góry powinno starczyć
      $scope.children = response.children;
    });
  };
  $scope.editionCancel = function (kid) {
    kid.edition = false;
  };

  Restangular.all('api/parents/' + $route.current.params.parentId + '/children').getList().then(function (kids) {
    $scope.children = kids;
  });
}]);

module.controller("RegistrationCtrl", ["$scope", "$rootScope", "$location", 'Restangular', function ($scope, $rootScope, $location, Restangular) {

  if (!$rootScope.$sessionStorage || !$rootScope.$sessionStorage.user) {

    $scope.user = {
      id: null,
      name: "",
      mail: "",
      phone: "",
      password: ""
    };

    $scope.validLogin = true;

    $scope.register = function () {
      var userToSend = angular.copy($scope.user);
      userToSend.password = CryptoJS.MD5(userToSend.password);
      Restangular.all("api/parents").customPUT(userToSend).then(function (response) {
        alert("Rejestracja zakończona sukcesem!");
        $rootScope.$sessionStorage.user = response;
        $location.path("/parents/" + $rootScope.$sessionStorage.user.id + "/children");

        $.ajaxSetup({
          beforeSend: function (xhr) {
            xhr.setRequestHeader("UserLogin", $rootScope.$sessionStorage.user.name);
          }
        });
      });
    };

    $scope.validateName = function (form) {
      Restangular.all("api/parents/" + $scope.user.name + "/validate").customGET().then(function (response) {

        response = response === "true";

        form.registrationForm.login.$setValidity("notUnique", response);

        $scope.validLogin = response;
      });
    }
  }

}]);

module.controller("LoginCtrl", ["$scope", "$rootScope", "$location", 'Restangular', function ($scope, $rootScope, $location, Restangular) {

  if (!$rootScope.$sessionStorage || !$rootScope.$sessionStorage.user) {

    $scope.user = {
      name: "",
      password: ""
    };

    $scope.login = function () {
      var userToSend = angular.copy($scope.user);
      userToSend.password = CryptoJS.MD5(userToSend.password).toString();
      Restangular.all("api/authentication/login").customPOST(userToSend).then(function (response) {
        $rootScope.$sessionStorage.user = response;
        $location.path("/parents/" + $rootScope.$sessionStorage.user.id + "/children");

        $.ajaxSetup({
          beforeSend: function (xhr) {
            xhr.setRequestHeader("UserLogin", $rootScope.$sessionStorage.user.name);
          }
        });
      });
    }
  }
}]);

module.directive("datePicker", function () {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      $(element).datetimepicker({
        format: attrs.format,
        timepicker: !attrs.tp || attrs.tp === "true",
        datepicker: !attrs.dp || attrs.dp === "true",
        onChangeDateTime: function () {
          $(element).trigger("input");
        }
      });
    }
  };
});

module.directive("shortMenu", function () {
  return {
    restrict: 'E',
    templateUrl: 'html/templates/short.html'
  };
});

//UserLogin

module.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
  GoogleMapApi.configure({
    key: 'AIzaSyCs43sxpfFvLthpjW08vmScG3SOmDaux04',
    v: '3.20',
    libraries: 'geometry,visualization,places'
  });
}]);

module.run(['$templateCache', '$rootScope', function ($templateCache, $rootScope) {
  $templateCache.put('searchbox.tpl.html', '<input id="pac-input" class="pac-controls" type="text" placeholder="Wyszukaj i wybierz lokalizaje do przeskoczenia" style="width:50%">');
  $templateCache.put('control.tpl.html', '<button class="tiny success radius" style="padding: 3px" ng-show="$root.anyChange" ng-click="saveClick()">Zapisz zmiany</button>');
  $templateCache.put('control2.tpl.html', '<button class="tiny alert radius" style="padding: 3px" ng-show="$root.anyChange" ng-click="cancelClick()">Cofnij zmiany</button>');

  $.ajaxSetup({
    beforeSend: function (xhr) {
      xhr.setRequestHeader("UserLogin", $rootScope.$sessionStorage.user.name);
    }
  });

}]);

module.controller('saveController', ['$scope', '$rootScope', function ($scope, $rootScope) {
  $rootScope.anyChange = false;
  $scope.$on("changeMade", function () {
    $rootScope.anyChange = true;
  });
  $scope.cancelClick = function () {
    $rootScope.anyChange = false;
    $scope.$emit("cancelPerformed");
  };
  $scope.saveClick = function () {
    $rootScope.anyChange = false;
    $scope.$emit("savePerformed");
  };
}]);

module.controller("MapCtrl", ['$scope', '$timeout', 'uiGmapLogger', '$http', 'uiGmapGoogleMapApi', 'Restangular', '$route', '$interval', 'SweetAlert',
  function ($scope, $timeout, $log, $http, GoogleMapApi, Restangular, $route, $interval, SweetAlert) {

    var AREA_COLOR = "#08B21F";
    var STROKE_COLOR = "#08B21F";
    var CLICKED_AREA = "#FF6633";
    var CLICKED_STROKE = "#CC0000";

    $scope.cycleOpts = [{id: "D", label: "dzień"}, {id: "W", label: "tydzień"}, {id: "M", label: "miesiąc"}];
    $scope.selectedCycle = null;
    $scope.cycleNumber = null;
    $scope.cycleChange = function () {
      if (!$scope.selectedCycle) {
        $scope.cycleNumber = null;
      }
    };

    $scope.childId = $route.current.params.childId;
    $scope.child = null;

    var tempDate = new Date();
    var local = new Date(tempDate);
    local.setMinutes(tempDate.getMinutes() - tempDate.getTimezoneOffset());
    $scope.mainDate = local.toJSON().slice(0, 10);

    var h = tempDate.getHours();
    $scope.startTime = (h < 10 ? "0" + h : h) + ":00";
    h += 2;
    $scope.endTime = (h < 10 ? "0" + h : (h > 23 ? "23" : h)) + (h > 23 ? ":59" : ":00");

    var resetScopeCircles = function () {
      $scope.currentCircle = {id: 0, radius: 100};
      $scope.currentMapCircle = {radius: 100};
      $scope.currentIndex = -1;
    };
    resetScopeCircles();

    var lastTimeout;
    var changeTimeout;
    $scope.changeRadius = function (val) {
      var currentTime = new Date().getTime();

      if (lastTimeout && lastTimeout + 1000 > currentTime) {
        $timeout.cancel(changeTimeout);
      }

      lastTimeout = currentTime;
      changeTimeout = $timeout(function () {
        $scope.currentCircle.radius = parseFloat(val);
        $scope.currentMapCircle.radius = parseFloat(val);
        $scope.$broadcast("changeMade");
      }, 1000);
    };

    $scope.deleteActiveCircle = function () {
      $scope.map.circles.remove($scope.currentIndex);
      resetScopeCircles();
      $scope.refresh = true;
      $scope.$broadcast("changeMade");
    };

    var lastStartTimeout;
    var changeStartTimeout;
    $scope.startChanged = function (val) {
      var currentTime = new Date().getTime();

      if (lastStartTimeout && lastStartTimeout + 3000 > currentTime) {
        $timeout.cancel(changeStartTimeout);
      }

      lastStartTimeout = currentTime;
      changeStartTimeout = $timeout(function () {
        if ($scope.currentCircle.areaDates) {
          var i;
          var ad = $scope.currentCircle.areaDates;
          for (i = 0; i < ad.length; ++i) {
            if (ad[i].startTime === $scope.clickedStartTime) {
              var tempD = $scope.mainDate.split("-");
              var tempT = val.split(":");
              ad[i].startTime = new Date(tempD[0], tempD[1] - 1, tempD[2], tempT[0], tempT[1], 0, 0).getTime();
            }
          }
          $scope.$broadcast("changeMade");
        }
      }, 3000);
    };

    var lastEndTimeout;
    var changeEndTimeout;
    $scope.endChanged = function (val) {
      var currentTime = new Date().getTime();

      if (lastEndTimeout && lastEndTimeout + 3000 > currentTime) {
        $timeout.cancel(changeEndTimeout);
      }

      lastEndTimeout = currentTime;
      changeEndTimeout = $timeout(function () {
        if ($scope.currentCircle.areaDates) {
          var i;
          var ad = $scope.currentCircle.areaDates;
          for (i = 0; i < ad.length; ++i) {
            if (ad[i].endTime === $scope.clickedEndTime) {
              var tempD = $scope.mainDate.split("-");
              var tempT = val.split(":");
              ad[i].endTime = new Date(tempD[0], tempD[1] - 1, tempD[2], tempT[0], tempT[1], 59, 999).getTime();
            }
          }
          $scope.$broadcast("changeMade");
        }
      }, 3000);
    };

    $scope.search = {events: {}};

    $scope.refresh = false;
    $scope.checkRefresh = function () {
      if ($scope.refresh) {
        $scope.refresh = false;
        return true;
      }
    };

    $scope.$on("savePerformed", function () {
      var areasToSave = [];
      $scope.map.circles.forEach(function (circle) {
        var adToSave = [];
        circle.areaDates.forEach(function (ad) {
          adToSave.push({
            id: ad.id,
            name: ad.name,
            startTime: ad.startTime,
            endTime: ad.endTime,
            alarm: ad.alarm
          });
        });
        areasToSave.push({
          id: circle.id,
          name: circle.name,
          latitude: circle.center.latitude,
          longitude: circle.center.longitude,
          radius: circle.radius / 1000.0,
          areaDates: adToSave
        });
      });
      Restangular.one('api/children', $scope.childId).all('areas').customPOST({areas: areasToSave}, "", {date: $scope.mainDate}).then(function (areas) {
        pushingAreas(areas);
      });
    });
    $scope.$on("cancelPerformed", function () {
      $scope.attachCircles();
    });

    // tu wolne
    var radiusChangedListener = function (area) {
      if ($scope.currentMapCircle.setOptions) {
        $scope.currentMapCircle.setOptions({
          fillColor: AREA_COLOR,
          strokeColor: STROKE_COLOR
        });
      }
      resetScopeCircles();
    };
    var clickCircleListener = function (area) {
      var cs = $scope.map.circles;
      var i;
      for (i = 0; i < cs.length; ++i) {
        if (cs[i].center.latitude === area.center.lat() && cs[i].center.longitude === area.center.lng()) {
          $scope.currentIndex = i;
          break;
        }
      }
      if ($scope.currentMapCircle.setOptions) {
        $scope.currentMapCircle.setOptions({
          fillColor: AREA_COLOR,
          strokeColor: STROKE_COLOR
        });
      }
      // podmianka na nowe kółko
      if (!_.isEqual($scope.currentCircle, cs[i])) {
        $scope.currentCircle = cs[i];
        $scope.currentMapCircle = area;
        $scope.currentMapCircle.setOptions({
          fillColor: CLICKED_AREA,
          strokeColor: CLICKED_STROKE
        });

        // mieszamy z datą
        if (cs[i].areaDates) {
          var dt = cs[i].areaDates;
          for (var j = 0; j < dt.length; ++j) {
            var date = new Date(dt[j].startTime);

            var month = date.getMonth() + 1;
            var day = date.getDay();
            if ($scope.mainDate === date.getFullYear() + "-" + month < 10 ? "0" + month : month + "-" + day < 10 ? "0" + day : day) {
              var h = date.getHours();
              var m = date.getMinutes();
              $scope.startTime = (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m);

              var date2 = new Date(dt[j].endTime);
              h = date2.getHours();
              m = date2.getMinutes();
              $scope.endTime = (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m);

              $scope.clickedStartTime = date.getTime();
              $scope.clickedEndTime = date2.getTime();

              break;
            }
          }
        }

      } else {
        resetScopeCircles();
      }
    };

    $scope.attachCircles = function () {
      if ($scope.childId && $scope.childId > 0) {
        Restangular.one('api/children', $scope.childId).customGET("areas", {date: $scope.mainDate}).then(function (areas) {
          pushingAreas(areas);
        });
      }
    };

    var pushingAreas = function (areas) {
      $scope.map.circles = [];

      areas.forEach(function (area) {
        $scope.map.circles.push({
            id: area.id,
            center: {
              latitude: area.latitude,
              longitude: area.longitude
            },
            radius: area.radius * 1000,
            stroke: {
              color: STROKE_COLOR,
              weight: 2,
              opacity: 1
            },
            fill: {
              color: AREA_COLOR,
              opacity: 0.5
            },
            geodesic: true, // optional: defaults to false
            draggable: true, // optional: defaults to false
            clickable: true, // optional: defaults to true
            editable: true, // optional: defaults to false
            visible: true, // optional: defaults to true
            events: {
              click: function (area) {
                clickCircleListener(area)
              },
              radius_changed: function (area) {
                radiusChangedListener(area)
              },
              center_changed: function () {
                $scope.$broadcast("changeMade");
              }
            },
            areaDates: area.areaDates
          }
        );
      });
    };

    var createMap = function () {
      angular.extend($scope, {
        map: {
          show: true,
          control: {},
          version: "uknown",
          center: {
            latitude: $route.current.params.y,
            longitude: $route.current.params.x
          },
          options: {
            streetViewControl: false,
            panControl: false,
            maxZoom: 20,
            minZoom: 3
          },
          zoom: $scope.childId > 0 ? 16 : 6,
          dragging: false,
          bounds: {},
          markers: [{
            id: $scope.child ? $scope.child.id : 1,
            latitude: $scope.child ? $scope.child.latitude : $route.current.params.y,
            longitude: $scope.child ? $scope.child.longitude : $route.current.params.x,
            showWindow: false,
            windowText: $scope.child ? $scope.child.name + " znajduje się w tym miejscu."
              : $route.current.params.x + ", " + $route.current.params.y,
            options: {
              animation: 1,
              labelContent: $scope.child ? $scope.child.name : 'Marker',
              labelAnchor: "22 0",
              labelClass: "marker-labels"
            }
          }],
          windowOptions: {
            boxClass: 'custom-info-window',
            closeBoxDiv: '<div" class="pull-right" style="position: relative; cursor: pointer; margin: -20px -15px;">X</div>',
            disableAutoPan: true
          },
          clickedMarker: {
            id: 0,
            options: {}
          },
          events: {
            // This turns of events and hits against scope from gMap events this does speed things up
            // adding a blacklist for watching your controller scope should even be better
            // blacklist: ['drag', 'dragend','dragstart','zoom_changed', 'center_changed'],
            tilesloaded: function (map, eventName, originalEventArgs) {
            },
            // wstawiamy tutaj kółko tak naprawdę
            click: function (mapModel, eventName, originalEventArgs) {

              var tempD = $scope.mainDate.split("-");
              var startTempT = $scope.startTime.split(":");
              var endTempT = $scope.endTime.split(":");

              var starts = [];
              var ends = [];
              starts.push(new Date(tempD[0], tempD[1] - 1, tempD[2], startTempT[0], startTempT[1], 0, 0).getTime());
              ends.push(new Date(tempD[0], tempD[1] - 1, tempD[2], endTempT[0], endTempT[1], 59, 999).getTime());

              if ($scope.selectedCycle && $scope.cycleNumber) {
                var D = $scope.selectedCycle === "D" ? 1 : ($scope.selectedCycle === "W" ? 7 : 0);
                var M = $scope.selectedCycle === "M" ? 1 : 0;
                for (var i = 1; i <= $scope.cycleNumber; ++i) {
                  var s = new Date(starts[i - 1]);
                  var e = new Date(ends[i - 1]);
                  starts.push(new Date(tempD[0], s.getMonth() + M, s.getDate() + D, startTempT[0], startTempT[1], 0, 0).getTime());
                  ends.push(new Date(tempD[0], e.getMonth() + M, e.getDate() + D, startTempT[0], startTempT[1], 59, 999).getTime());
                }
              }

              var areaDates = [];
              if (starts.length === ends.length) {
                for (var i = 0; i < starts.length; ++i) {
                  areaDates.push({
                    startTime: starts[i],
                    endTime: ends[i]
                  });
                }
              }

              // jak jest kółko, to je po prostu odkliknąć
              if ($scope.currentIndex > -1) {
                if ($scope.currentMapCircle.setOptions) {
                  $scope.currentMapCircle.setOptions({
                    fillColor: AREA_COLOR,
                    strokeColor: STROKE_COLOR
                  });
                }
                resetScopeCircles();
              } else {
                // 'this' is the directive's scope
                var e = originalEventArgs[0];

                $scope.currentCircle.radius = parseFloat($scope.currentCircle.radius);
                $scope.map.circles.push({
                  center: {
                    latitude: e.latLng.lat(),
                    longitude: e.latLng.lng()
                  },
                  radius: $scope.currentCircle.radius > 0 ? $scope.currentCircle.radius : 0, // przed wysłaniem podzielić przez 1000
                  stroke: {
                    color: STROKE_COLOR,
                    weight: 2,
                    opacity: 1
                  },
                  fill: {
                    color: AREA_COLOR,
                    opacity: 0.5
                  },
                  geodesic: true, // optional: defaults to false
                  draggable: true, // optional: defaults to false
                  editable: true, // optional: defaults to false
                  events: {
                    click: function (area) {
                      clickCircleListener(area)
                    },
                    radius_changed: function (area) {
                      radiusChangedListener(area)
                    },
                    center_changed: function () {
                      $scope.$broadcast("changeMade");
                    }
                  },
                  areaDates: areaDates
                });
                $scope.$broadcast("changeMade");
                $scope.refresh = true;
                $scope.$apply();
              }
            }
          },

          circles: []
        }
      });

      $scope.map.markers.forEach(function (marker) {
        marker.closeClick = function () {
          marker.showWindow = false;
          $scope.$evalAsync();
        };
        marker.onClicked = function () {
          marker.showWindow = true;
          $scope.$apply();
        };
      });
      $scope.clackMarker = function (gMarker, eventName, model) {
        alert("clackMarker: " + model);
        $log.log("from clackMarker");
        $log.log(model);
      };
    };

    // -------------- MAIN PART -------------- \\
    GoogleMapApi.then(function (maps) {
      maps.visualRefresh = true;

      Restangular.one('api/children/', $scope.childId).get().then(function (kid) {
        $scope.child = kid;

        var alert = {
          title: kid.name + " jest poza zleconym obszarem!",
          text: "Upewnij się, że czas i obszar były zdefiniowane poprawnie i skontaktuj się ze swoim dzieckiem jak najszybciej!",
          type: "warning",
          showCancelButton: false,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ok",
          closeOnConfirm: true
        };

        var refresh = function () {
          Restangular.one('api/children/', $scope.childId).get().then(function (kid) {
            $scope.child = kid;
            $scope.map.markers[0].latitude = kid.latitude;
            $scope.map.markers[0].longitude = kid.longitude;

            if (kid.alarm) {
              SweetAlert.swal(alert);
              $interval.cancel(promise);
              promise = $interval(refresh, 5000);
            }
          });
        };
        var promise = $interval(refresh, 30000);

        if (kid.alarm) {
          SweetAlert.swal(alert);

          $interval.cancel(promise);
          promise = $interval(refresh, 5000);
        }

        createMap();
        $scope.attachCircles();

        $scope.search = {
          options: {
            autocomplete: true
          },
          html: "searchbox.tpl.html",
          events: {
            place_changed: function (autocomplete) {
              var place = autocomplete.getPlace();

              if (place.address_components) {

                var bounds = new google.maps.LatLngBounds();

                $scope.map.center.latitude = place.geometry.location.lat();
                $scope.map.center.longitude = place.geometry.location.lng();

                bounds.extend(place.geometry.location);

                $scope.map.bounds = {
                  northeast: {
                    latitude: bounds.getNorthEast().lat(),
                    longitude: bounds.getNorthEast().lng()
                  },
                  southwest: {
                    latitude: bounds.getSouthWest().lat(),
                    longitude: bounds.getSouthWest().lng()
                  }
                };
                $scope.map.zoom = 15;
              }
            }
          }
        };

        $timeout(function () {
        }, 20000);
      }, function () {
        createMap();
      });
    });
  }]);