package dziecko.rests;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dziecko.models.Area;
import dziecko.models.Child;
import dziecko.models.dtos.AreaList;
import dziecko.models.dtos.ChildValidationDto;
import dziecko.services.ChildService;

@RestController
@RequestMapping("/children")
public class ChildRest {

    @Autowired
    ChildService childService;

    @RequestMapping(value = "/{childId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Child> getChild(@PathVariable("childId") Integer childId) {

        return new ResponseEntity<Child>(childService.getChild(childId), HttpStatus.OK);
    }

    @RequestMapping(value = "/{childId}/areas", method = RequestMethod.GET, params = {"date" }, produces = "application/json")
    public ResponseEntity<Set<Area>> getAreasForChildAndData(@PathVariable("childId") Integer childId,
            @RequestParam(value = "date") String date) {

        return new ResponseEntity<Set<Area>>(childService.getAreasForChildAndData(childId, date), HttpStatus.OK);
    }

    @RequestMapping(value = "/{childId}/areas", method = RequestMethod.POST, params = {"date" })
    public ResponseEntity<Set<Area>> setAreasForChildAndData(@PathVariable("childId") Integer childId,
            @RequestParam(value = "date") String date, @RequestBody AreaList newAreas) {

        return new ResponseEntity<Set<Area>>(childService.setAreasForChildAndData(childId, date, newAreas.getAreas()),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/validate", method = RequestMethod.GET, params = {"token", "latitude", "longitude" }, produces = "application/json")
    public ResponseEntity<ChildValidationDto> validateIfChildIsOutOfArea(@RequestParam("token") String token,
            @RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude) {
        return new ResponseEntity<ChildValidationDto>(childService.processValidationForMobile(token, latitude, longitude, 2),
                HttpStatus.OK);
    }
    
    @RequestMapping(value = "{token}/confirmation", method = RequestMethod.GET, params = {"value"})
    public ResponseEntity<Void> confirmationResponse(@PathVariable("token") String token, @RequestParam("value") Boolean value) {
    	childService.confirmation(token, value);
    	return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
