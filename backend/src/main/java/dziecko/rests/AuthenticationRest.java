package dziecko.rests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dziecko.models.Parent;
import dziecko.models.login.LoginRequestBody;
import dziecko.services.AuthenticationService;

@RestController
@RequestMapping("/authentication")
public class AuthenticationRest {

    @Autowired
    AuthenticationService authService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<Parent> login(@RequestBody LoginRequestBody data) {

        return new ResponseEntity<Parent>(authService.authenticate(data.getName(), data.getPassword()), HttpStatus.OK);
    }

    @RequestMapping(value = "/login/mobile", method = RequestMethod.GET, params = {"name", "password" })
    public ResponseEntity<Integer> mobileLogin(@RequestParam("name") String name, @RequestParam("password") String pass) {

        return new ResponseEntity<Integer>(authService.authenticateMobile(name, pass), HttpStatus.OK);
    }

    @RequestMapping(value = "/logout/{username}", method = RequestMethod.DELETE)
    public ResponseEntity<String> logout(@PathVariable("username") String username) {

        authService.deauthenticate(username);

        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
