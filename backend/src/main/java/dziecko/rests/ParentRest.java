package dziecko.rests;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dziecko.models.Child;
import dziecko.models.Parent;
import dziecko.models.dtos.PairChildDto;
import dziecko.models.dtos.ValidatedChildDto;
import dziecko.services.ParentService;

@RestController
@RequestMapping("/parents")
public class ParentRest {

    @Autowired
    ParentService parentService;

    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<Parent> createParent(@RequestBody Parent data) {
        return new ResponseEntity<Parent>(parentService.createParent(data), HttpStatus.OK);

    }

    @RequestMapping(value = "/{parentId}/children", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Set<Child>> readChildrenForParent(@PathVariable("parentId") Integer parentId) {
        return new ResponseEntity<Set<Child>>(parentService.readChildrenForParent(parentId), HttpStatus.OK);
    }

    @RequestMapping(value = "/{parentId}/children", method = RequestMethod.PUT)
    public ResponseEntity<Parent> createChildForParent(@PathVariable("parentId") Integer parentId,
            @RequestBody Child data) {
        return new ResponseEntity<Parent>(parentService.createChildrenForParent(parentId, data), HttpStatus.OK);
    }

    @RequestMapping(value = "/{parentId}/children", method = RequestMethod.POST)
    public ResponseEntity<Parent> updateChildForParent(@PathVariable("parentId") Integer parentId,
            @RequestBody Child data) {
        return new ResponseEntity<Parent>(parentService.updateChildrenForParent(parentId, data), HttpStatus.OK);
    }

    @RequestMapping(value = "/pair", method = RequestMethod.GET, params = {"parentName", "password", "childName" }, produces = "application/json")
    public ResponseEntity<PairChildDto> validateParentsDataAndGetChildToken(
            @RequestParam("parentName") String parentName, @RequestParam("password") String password,
            @RequestParam("childName") String childName) {
        return new ResponseEntity<PairChildDto>(parentService.getChildToken(parentName, password, childName),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/{parentName}/validate", method = RequestMethod.GET)
    public ResponseEntity<Boolean> validateName(@PathVariable("parentName") String name) {
        return new ResponseEntity<Boolean>(parentService.checkIfNotExists(name), HttpStatus.OK);
    }

    @RequestMapping(value = "/{parentId}/children/validate", method = RequestMethod.GET)
    public ResponseEntity<List<ValidatedChildDto>> validateName(@PathVariable("parentId") Integer parentId) {
        return new ResponseEntity<List<ValidatedChildDto>>(parentService.validateAllChildren(parentId), HttpStatus.OK);
    }

    @RequestMapping(value = "/{parentId}/children/{childName}/requestConfirmation", method = RequestMethod.GET)
    public ResponseEntity<Void> requestConfirmation(@PathVariable("parentId") Integer parentId,
            @PathVariable("childName") String childName) {
    	parentService.requestConfirmation(parentId, childName);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
