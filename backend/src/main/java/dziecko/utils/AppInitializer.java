package dziecko.utils;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import dziecko.models.Area;
import dziecko.models.AreaDate;
import dziecko.models.Child;
import dziecko.repositories.ParentRepository;
import dziecko.utils.builders.AreaBuilder;
import dziecko.utils.builders.AreaDateBuilder;
import dziecko.utils.builders.ChildBuilder;
import dziecko.utils.builders.ParentBuilder;

@Component
@Profile("!test")
public class AppInitializer {

    @Autowired
    private ParentRepository parentRepository;

    private static final long HOUR = 3600 * 1000; // in milli-seconds.
    private Date date = new Date();

    @PostConstruct
    public void fillDatabase() {

        AreaDate betweenNowAndNowPlus2h = new AreaDateBuilder().withStartTime(date)
                .withEndTime(new Date(date.getTime() + 2 * HOUR)).build();

        Area area1 = new AreaBuilder().withLatitiude(51.26705725207446).withLongtitiude(22.52069592475891)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build(); // radius in
        // km
        Area area2 = new AreaBuilder().withLatitiude(51.2658891858365).withLongtitiude(22.520095109939575)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area3 = new AreaBuilder().withLatitiude(51.26481507573857).withLongtitiude(22.52071738243103)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area4 = new AreaBuilder().withLatitiude(51.264184024349866).withLongtitiude(22.52318501472473)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area5 = new AreaBuilder().withLatitiude(51.26367380625418).withLongtitiude(22.52543807029724)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area6 = new AreaBuilder().withLatitiude(51.26297560389227).withLongtitiude(22.527412176132202)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area7 = new AreaBuilder().withLatitiude(51.26211626334849).withLongtitiude(22.529386281967163)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area8 = new AreaBuilder().withLatitiude(51.26104206507564).withLongtitiude(22.530630826950073)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area9 = new AreaBuilder().withLatitiude(51.259658997832254).withLongtitiude(22.531660795211792)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area10 = new AreaBuilder().withLatitiude(51.258839880135795).withLongtitiude(22.53344178199768)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area11 = new AreaBuilder().withLatitiude(51.25834303114282).withLongtitiude(22.535158395767212)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.15).build();

        Child kid = new ChildBuilder().withRelationToArea(area1).withRelationToArea(area2).withRelationToArea(area3)
                .withRelationToArea(area4).withRelationToArea(area5).withRelationToArea(area6)
                .withRelationToArea(area7).withRelationToArea(area8).withRelationToArea(area9)
                .withRelationToArea(area10).withRelationToArea(area11).withName("Jacek").withIsImportant(false)
                .withAlarm(false).withLatitiude(51.26705725207446).withLongtitiude(22.52069592475891)
                .withToken("tokenJacek").build();

        Child kid2 = new ChildBuilder().withLatitiude(51.26705725207446).withLongtitiude(22.52069592475891)
                .withName("Agatka").withToken("tokenAgatka").build();

        // MD5 password
        parentRepository.save(new ParentBuilder().withName("user").withPassword("ee11cbb19052e40b07aac0ca060c23ee")
                .withRelationToChild(kid).withRelationToChild(kid2).withMail("mat3e@interia.pl").build());
    }
}
