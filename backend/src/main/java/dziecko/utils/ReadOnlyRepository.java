package dziecko.utils;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface ReadOnlyRepository<T, ID extends Serializable> extends Repository<T, ID> {
    /**
     * Returns the number of entities available.
     *
     * @return the number of entities
     */
    long count();

    /**
     * Returns whether an entity with the given id exists. This does not care if the entry is marked as deleted or not.
     *
     * @param id
     *            must not be {@literal null}.
     * @return true if an entity with the given id exists, {@literal false} otherwise
     */
    boolean exists(ID id);

    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    Iterable<T> findAll();

    /**
     * Returns all instances of the type with the given IDs.
     *
     * @param ids
     *            IDs
     * @return all entities with the given IDs
     */
    List<T> findAll(Iterable<ID> ids);

    /**
     * Returns a {@link org.springframework.data.domain.Page} of entities meeting the paging restriction provided in the
     * {@code Pageable} object.
     *
     * @param pageable
     *            page information
     * @return a page of entities
     */
    List<T> findAll(Pageable pageable);

    /**
     * Returns all entities sorted by the given options.
     *
     * @param sort
     *            sort information
     * @return all entities sorted by the given options
     */
    List<T> findAll(Sort sort);

    /**
     * Retrieves an entity by its id.
     *
     * @param id
     *            must not be {@literal null}.
     * @return the entity with the given id or {@literal null} if none found
     */
    T findOne(ID id);

    /**
     * Returns a reference to the entity with the given identifier.
     *
     * @param id
     *            must not be {@literal null}.
     * @return a reference to the entity with the given identifier.
     * @see javax.persistence.EntityManager.EntityManager#getReference(Class, Object)
     */
    T getOne(ID id);
}
