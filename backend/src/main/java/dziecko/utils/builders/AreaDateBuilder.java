package dziecko.utils.builders;

import java.util.Date;

import org.springframework.test.util.ReflectionTestUtils;

import dziecko.models.AreaDate;

public class AreaDateBuilder {

    private AreaDate buildingResult;

    public AreaDateBuilder() {
        buildingResult = new AreaDate();
    }

    public AreaDateBuilder withId(Long id) {
        ReflectionTestUtils.setField(buildingResult, "id", id);
        return this;
    }

    public AreaDateBuilder withStartTime(Date date) {
        ReflectionTestUtils.setField(buildingResult, "startTime", date);
        return this;
    }

    public AreaDateBuilder withEndTime(Date date) {
        ReflectionTestUtils.setField(buildingResult, "endTime", date);
        return this;
    }

    public AreaDate build() {
        return buildingResult;
    }

}
