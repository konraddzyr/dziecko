package dziecko.utils.builders;

import java.util.Set;

import org.springframework.test.util.ReflectionTestUtils;

import dziecko.models.Child;
import dziecko.models.Parent;

public class ParentBuilder {

    private Parent buildingResult;

    public ParentBuilder() {
        buildingResult = new Parent();
    }

    public ParentBuilder withId(Long id) {
        ReflectionTestUtils.setField(buildingResult, "id", id);
        return this;
    }

    public ParentBuilder withName(String name) {
        ReflectionTestUtils.setField(buildingResult, "name", name);
        return this;
    }

    public ParentBuilder withPassword(String pass) {
        ReflectionTestUtils.setField(buildingResult, "password", pass);
        return this;
    }

    public ParentBuilder withMail(String mail) {
        ReflectionTestUtils.setField(buildingResult, "mail", mail);
        return this;
    }

    public ParentBuilder withRelationToChild(Child child) {
        Set<Child> children = buildingResult.getChildren();
        children.add(child);
        ReflectionTestUtils.setField(buildingResult, "children", children);
        return this;
    }

    public Parent build() {
        return buildingResult;
    }

}
