package dziecko.utils.builders;

import java.util.Set;

import org.springframework.test.util.ReflectionTestUtils;

import dziecko.models.Area;
import dziecko.models.Child;

public class ChildBuilder {

    private Child buildingResult;

    public ChildBuilder() {
        buildingResult = new Child();
    }

    public ChildBuilder withId(Long id) {
        ReflectionTestUtils.setField(buildingResult, "id", id);
        return this;
    }

    public ChildBuilder withName(String name) {
        ReflectionTestUtils.setField(buildingResult, "name", name);
        return this;
    }

    public ChildBuilder withLatitiude(Double latitude) {
        ReflectionTestUtils.setField(buildingResult, "latitude", latitude);
        return this;
    }

    public ChildBuilder withLongtitiude(Double longitude) {
        ReflectionTestUtils.setField(buildingResult, "longitude", longitude);
        return this;
    }

    public ChildBuilder withToken(String token) {
        ReflectionTestUtils.setField(buildingResult, "token", token);
        return this;
    }

    public ChildBuilder withIsImportant(boolean flag) {
        ReflectionTestUtils.setField(buildingResult, "isImportant", flag);
        return this;
    }

    public ChildBuilder withAlarm(boolean alarm) {
        ReflectionTestUtils.setField(buildingResult, "alarm", alarm);
        return this;
    }

    public ChildBuilder withRelationToArea(Area area) {
        Set<Area> areas = buildingResult.getAreas();
        areas.add(area);
        ReflectionTestUtils.setField(buildingResult, "areas", areas);
        return this;
    }

    public Child build() {
        return buildingResult;
    }

}
