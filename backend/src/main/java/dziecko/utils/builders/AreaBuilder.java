package dziecko.utils.builders;

import java.util.List;

import org.springframework.test.util.ReflectionTestUtils;

import dziecko.models.Area;
import dziecko.models.AreaDate;

public class AreaBuilder {

    private Area buildingResult;

    public AreaBuilder() {
        buildingResult = new Area();
    }

    public AreaBuilder withId(Long id) {
        ReflectionTestUtils.setField(buildingResult, "id", id);
        return this;
    }

    public AreaBuilder withLatitiude(Double latitude) {
        ReflectionTestUtils.setField(buildingResult, "latitude", latitude);
        return this;
    }

    public AreaBuilder withLongtitiude(Double longitude) {
        ReflectionTestUtils.setField(buildingResult, "longitude", longitude);
        return this;
    }

    public AreaBuilder withRadius(Double radius) {
        ReflectionTestUtils.setField(buildingResult, "radius", radius);
        return this;
    }

    public AreaBuilder withRelationToDate(AreaDate areaDate) {
        List<AreaDate> dates = buildingResult.getAreaDates();
        dates.add(areaDate);
        ReflectionTestUtils.setField(buildingResult, "areaDates", dates);
        return this;
    }

    public Area build() {
        return buildingResult;
    }

}
