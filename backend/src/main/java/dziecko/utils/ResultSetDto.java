package dziecko.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResultSetDto implements Serializable {

    /** The Constant serialVersionUID required by Serializable. */
    private static final long serialVersionUID = -609969942797447374L;

    private List<String> columns;

    private List<RowDto> rows;

    private long rowsCount;

    /**
     * Default constructor.
     */
    public ResultSetDto() {

    }

    /**
     *
     * @param columns
     *            columns list
     * @param rows
     *            rows list
     */
    public ResultSetDto(List<String> columns, List<RowDto> rows) {
        this.columns = columns;
        this.rows = rows;
        this.rowsCount = rows.size();
    }

    /**
     * Instantiates a new result set dto.
     *
     * @param columns
     *            the columns
     * @param rows
     *            the rows
     * @param rowsCount
     *            the rows count
     */
    public ResultSetDto(List<String> columns, List<RowDto> rows, long rowsCount) {
        this.columns = columns;
        this.rows = rows;
        this.rowsCount = rowsCount;
    }

    /**
     * @return the columns
     */
    public List<String> getColumns() {
        return this.columns;
    }

    /**
     * @param index
     *            index
     * @return row as {@link Map}
     *         <p>
     *         <b>Caution!</b> Throws IndexOutOfBoundsException if the index is out of range (
     *         <tt>index &lt; 0 || index &gt;= size()</tt> )
     *         </p>
     */
    public Map<String, Object> getRow(int index) {
        List<Object> listRow = this.rows.get(index).getData();
        Map<String, Object> row = new HashMap<>();

        for (int i = 0; i < this.columns.size(); i++) {
            row.put(this.columns.get(i), listRow.get(i));
        }

        return row;
    }

    /**
     * @return the rows
     */
    public List<RowDto> getRows() {
        return this.rows;
    }

    /**
     * @param columns
     *            the columns to set
     */
    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    /**
     * @param rows
     *            the rows to set
     */
    public void setRows(List<RowDto> rows) {
        this.rows = rows;
    }

    /**
     *
     * @return size
     */
    public int size() {
        return this.rows.size();
    }

    public boolean isEmpty() {
        return this.rows.isEmpty();
    }

    @Override
    public String toString() {
        return String.format("columns = %s%nrows = %s", this.columns.toString(), this.rows.toString());
    }

    /**
     * Gets the elements number.
     *
     * @return the elements number
     */
    public long getRowsCount() {
        return rowsCount;
    }

    /**
     * Sets the elements number.
     *
     * @param elementsNumber
     *            the new elements number
     */
    public void setRowsCount(long elementsNumber) {
        this.rowsCount = elementsNumber;
    }
}
