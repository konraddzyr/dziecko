package dziecko.utils;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseCrudRepository<T, ID extends Serializable> extends ReadOnlyRepository<T, ID> {
    /**
     * Deletes the entity with the given id.
     *
     * @param id
     *            must not be {@literal null}.
     */
    void delete(ID id);

    /**
     * Deletes the given entities.
     *
     * @param entities
     *            entities to delete
     */
    void delete(Iterable<? extends T> entities);

    /**
     * Deletes a given entity.
     *
     * @param entity
     *            entity to delete
     */
    void delete(T entity);

    /**
     * Deletes all entities managed by the repository.
     */
    void deleteAll();

    /**
     * Deletes all entites in a batch call.
     */
    void deleteAllInBatch();

    /**
     * Deletes the given entities in a batch which means it will create a single.
     * {@link org.springframework.data.jpa.repository.Query}. Assume that we will clear the
     * {@link javax.persistence.EntityManager} after the call.
     *
     * @param entities
     *            entities to delete
     */
    void deleteInBatch(Iterable<T> entities);

    /**
     * Flushes all pending changes to the database.
     */
    void flush();

    /**
     * Saves all given entities.
     *
     * @param <S>
     *            entity class
     * @param entities
     *            entities to save
     * @return the saved entities
     */
    <S extends T> List<S> save(Iterable<S> entities);

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed
     * the entity instance completely.
     *
     * @param <S>
     *            entity class
     * @param entity
     *            entity to save
     * @return the saved entity
     */
    <S extends T> S save(S entity);

    /**
     * Saves an entity and flushes changes instantly.
     *
     * @param entity
     *            enetity to save
     * @return the saved entity
     */
    T saveAndFlush(T entity);
}
