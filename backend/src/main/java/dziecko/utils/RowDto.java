package dziecko.utils;

import java.io.Serializable;
import java.util.List;

public class RowDto implements Serializable {

    /** The Constant serialVersionUID required by Serializable. */
    private static final long serialVersionUID = 2152040030507969413L;
    private List<Object> data;

    public RowDto() {
    }

    /**
     * @param data
     *            the data to set
     */
    public RowDto(List<Object> data) {
        this.data = data;
    }

    /**
     * @return the data
     */
    public List<Object> getData() {

        return this.data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(List<Object> data) {
        this.data = data;
    }
}
