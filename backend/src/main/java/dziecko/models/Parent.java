package dziecko.models;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.SortNatural;

import dziecko.enums.Roles;
import dziecko.models.dtos.ParentDto;

@Entity
@Table(name = "USERS", uniqueConstraints = @UniqueConstraint(columnNames = {"name" }))
// TODO: delete User and handle roles differently
public class Parent extends User implements DtoGenerator<ParentDto> {

    private static final long serialVersionUID = -4713999154261809839L;

    @Column(name = "MAIL", length = 32)
    private String mail;

    @Column(name = "PHONE", length = 9)
    private String phone;

    @OrderBy("id ASC")
    @SortNatural
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "USERS_CHILDREN", joinColumns = {@JoinColumn(name = "USER_ID", nullable = false, updatable = false) }, inverseJoinColumns = {@JoinColumn(name = "CHILD_ID", nullable = false, updatable = false) })
    private Set<Child> children = new LinkedHashSet<>();

    public Parent() {
        super();
    }

    public Parent(Integer id, String name, String password, String mail, String phone) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.mail = mail;
        this.phone = phone;
        this.role = Roles.PARENT;
    }

    public Set<Child> getChildren() {
        return this.children;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public ParentDto createDto() {
        return new ParentDto(id, name, password, mail, phone);
    }

    public void addChild(Child child) {
        if (child != null) {
            children.add(child);
        }
    }

    public void updateChild(Child child) {
        if (child != null) {
            Iterator<Child> it = children.iterator();

            while (it.hasNext()) {
                Child curr = it.next();
                if (curr.getId() == child.getId()) {
                    // only name can be changed
                    curr.setName(child.getName());
                }
            }
        }
    }

}
