package dziecko.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import dziecko.models.dtos.AreaDateDto;

@Entity
@Table(name = "AREADATES")
public class AreaDate extends AbstractBase implements DtoGenerator<AreaDateDto> {

    private static final long serialVersionUID = -2956040373671316338L;

    @Column(name = "START_TIME")
    private Date startTime;

    @Column(name = "END_TIME")
    private Date endTime;

    @Column(name = "ALARM", nullable = true)
    private Boolean alarm;

    public AreaDate() {

    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public Boolean getAlarm() {
        return alarm;
    }

    @Override
    public AreaDateDto createDto() {
        return new AreaDateDto(id, startTime, endTime, alarm);
    }

}
