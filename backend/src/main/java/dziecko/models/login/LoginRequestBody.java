package dziecko.models.login;

import java.io.Serializable;

public class LoginRequestBody implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private String password;

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

}
