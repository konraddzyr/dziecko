package dziecko.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import dziecko.models.dtos.NewsDto;

@Entity
@Table(name = "NEWS")
public class News extends AbstractBase implements DtoGenerator<NewsDto> {

    private static final long serialVersionUID = -2583725168157437442L;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "TIMESTAMP")
    private Date timestamp;

    public News() {
    }

    public News(String name, String description) {
        this.name = name;
        this.description = description;
        this.timestamp = new Date();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public NewsDto createDto() {
        return new NewsDto(id, name);
    }

}
