package dziecko.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import dziecko.enums.Roles;

@MappedSuperclass
public class User extends AbstractBase {

    private static final long serialVersionUID = 86835500387460413L;

    @Column(name = "PASSWORD", length = 32)
    protected String password;

    @Column(name = "ROLE")
    protected Roles role;

    public User() {
    }

    public User(String login, String password) {
        this.name = login;
        this.password = password;
        this.role = Roles.ADMIN;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Roles getRole() {
        return role;
    }

}
