package dziecko.models;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.SortNatural;

import com.fasterxml.jackson.annotation.JsonIgnore;

import dziecko.models.dtos.ChildDto;

@Entity
@Table(name = "CHILDREN")
public class Child extends AbstractBase implements DtoGenerator<ChildDto> {

    private static final long serialVersionUID = 570154846932351098L;

    @Column(name = "TOKEN", nullable = true)
    private String token;

    // indicates if we should update lat- and longitude each time, when checking coordinates
    @Column(name = "IS_IMPORTANT", nullable = true)
    private Boolean isImportant = false;

    // indicates if child is out of areas, determines isImportant = true
    @Column(name = "ALARM", nullable = true)
    private Boolean alarm = false;

    @Column(name = "CONFIRMATION_REQUIRED", nullable = true)
    private Boolean confirmationRequired = false;
    
    @Column(name = "CHILD_RESPONSED", nullable = true)
    private Boolean childResponsed = false;
    
    @Column(name = "RESPONSE", nullable = true)
    private Boolean response = false;

    @Column(name = "LATITUDE", nullable = true)
    private Double latitude;

    @Column(name = "LONGITUDE", nullable = true)
    private Double longitude;

    @JsonIgnore
    @ManyToMany(mappedBy = "children")
    private Set<Parent> parents;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "CHILD_ID")
    @OrderBy("id ASC")
    @SortNatural
    private Set<Area> areas = new HashSet<>();

    public Child() {
    }

    public Child(String name) {
        this.name = name;
        this.token = UUID.randomUUID().toString();
    }

    public Child(String name, String token) {
        this.name = name;
        this.token = token != null && !token.isEmpty() ? token : UUID.randomUUID().toString();
    }

    public Child(String name, String token, Boolean isImportant, Double latitude, Double longitude) {
        this(name, token);
        this.isImportant = isImportant;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Child(Integer id, String name, String token, Boolean isImportant, Double latitude, Double longitude) {
        this(name, token, isImportant, latitude, longitude);
        this.id = id;
    }

    public Boolean getIsImportant() {
        return isImportant;
    }

    public Boolean getAlarm() {
        return alarm;
    }    

    public Boolean getChildResponsed() {
		return childResponsed;
	}

	public Boolean getResponse() {
		return response;
	}

	public void setAlarm(Boolean alarm) {
        this.alarm = alarm;
        if (alarm) {
            this.isImportant = alarm;
        }
    }
	
	public void setChildResponsed(Boolean childResponsed) {
		this.childResponsed = childResponsed;
	}

	public void setResponse(Boolean response) {
		this.response = response;
	}

    public void setIsImportant(Boolean isImportant) {
        this.isImportant = isImportant;
    }

    public Boolean getConfirmationRequired() {
        return confirmationRequired;
    }

    public void setConfirmationRequired(Boolean confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getToken() {
        return token;
    }

    public Set<Area> getAreas() {
        return areas;
    }

    public Set<Parent> getParents() {
        return parents;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void removeAreas(Collection<Area> areas) {
        this.areas.removeAll(areas);
    }

    public void addAreas(Collection<Area> areas) {
        this.areas.addAll(areas);
    }

    @Override
    public ChildDto createDto() {
        return new ChildDto(id, name, token, isImportant, latitude, longitude);
    }

}
