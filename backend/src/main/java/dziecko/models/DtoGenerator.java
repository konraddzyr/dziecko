package dziecko.models;

import dziecko.models.dtos.AbstractDto;

public interface DtoGenerator<T extends AbstractDto> {
    public T createDto();
}
