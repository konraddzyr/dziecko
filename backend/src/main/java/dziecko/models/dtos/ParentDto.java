package dziecko.models.dtos;

import dziecko.models.Parent;

public class ParentDto extends AbstractDto implements EntityGenerator<Parent> {

    private static final long serialVersionUID = -938592475898016648L;

    private String password;
    private String mail;
    private String phone;

    public ParentDto(Integer id, String name, String password, String mail, String phone) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.mail = mail;
        this.phone = phone;
    }

    @Override
    public Parent createEntity() {
        return new Parent(id, name, password, mail, phone);
    }

}
