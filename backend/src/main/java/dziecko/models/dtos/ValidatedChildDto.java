package dziecko.models.dtos;

public class ValidatedChildDto {
	private String name;
	private Boolean alarm;
	private Boolean childResponsed;
	private Boolean response;

	public ValidatedChildDto(String name, Boolean alarm, Boolean childResponsed, Boolean response) {
		this.name = name;
		this.alarm = alarm;
		this.childResponsed = childResponsed;
		this.response = response;
	}

	public String getName() {
		return name;
	}

	public Boolean getAlarm() {
		return alarm;
	}

	public Boolean getChildResponsed() {
		return childResponsed;
	}

	public Boolean getResponse() {
		return response;
	}
}
