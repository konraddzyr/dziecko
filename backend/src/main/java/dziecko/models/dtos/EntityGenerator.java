package dziecko.models.dtos;

import dziecko.models.AbstractBase;

public interface EntityGenerator<T extends AbstractBase> {

    public T createEntity();
}
