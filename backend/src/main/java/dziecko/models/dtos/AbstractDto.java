package dziecko.models.dtos;

import java.io.Serializable;

public class AbstractDto implements Serializable {

    private static final long serialVersionUID = -4822715873235589121L;

    protected Integer id;
    protected String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
