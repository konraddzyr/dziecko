package dziecko.models.dtos;

import java.util.Date;

import dziecko.models.AreaDate;

public class AreaDateDto extends AbstractDto implements EntityGenerator<AreaDate> {

    private static final long serialVersionUID = -7549534550071221251L;

    private Date startTime;

    private Date endTime;

    private Boolean alarm;

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public Boolean getAlarm() {
        return alarm;
    }

    public AreaDateDto(Integer id, Date start, Date end, Boolean alarm) {
        this.id = id;
        this.startTime = start;
        this.endTime = end;
        this.alarm = alarm;
    }

    @Override
    public AreaDate createEntity() {
        return new AreaDate();
    }

}
