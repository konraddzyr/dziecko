package dziecko.models.dtos;

import dziecko.models.Area;
import dziecko.models.AreaDate;

public class AreaDto extends AbstractDto implements EntityGenerator<Area> {

    private static final long serialVersionUID = -6641255865950044960L;

    private Double latitude;

    private Double longitude;

    // should be in km
    private Double radius;

    AreaDate areaDate = null;

    public AreaDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public AreaDto(Integer id, String name, Double latitude, Double longitude, Double radius, AreaDate areaDate) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;

        this.areaDate = areaDate;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getRadius() {
        return radius;
    }

    @Override
    public Area createEntity() {
        return new Area();
    }

}
