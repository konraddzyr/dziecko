package dziecko.models.dtos;

public class PairChildDto {
	private boolean newRegistered;
	private String token;

	public PairChildDto(boolean newRegistered, String token) {
		this.newRegistered = newRegistered;
		this.token = token;
	}

	public boolean isNewRegistered() {
		return newRegistered;
	}

	public String getToken() {
		return token;
	}
	
}
