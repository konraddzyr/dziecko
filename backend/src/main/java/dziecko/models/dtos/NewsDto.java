package dziecko.models.dtos;

import dziecko.models.News;

public class NewsDto extends AbstractDto implements EntityGenerator<News> {

    private static final long serialVersionUID = 6348682720340759633L;

    public NewsDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public News createEntity() {
        return new News();
    }

}
