package dziecko.models.dtos;

import dziecko.models.Child;

public class ChildDto extends AbstractDto implements EntityGenerator<Child> {

    static final long serialVersionUID = -5893311083432815303L;

    private String token;
    private Boolean isImportant = false;
    private Double latitude;
    private Double longitude;

    public ChildDto(Integer id, String name, String token, Boolean isImportant, Double latitude, Double longitude) {
        this.id = id;
        this.name = name;
        this.token = token;
        this.isImportant = isImportant;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public Child createEntity() {
        return new Child(id, name, token, isImportant, latitude, longitude);
    }
}
