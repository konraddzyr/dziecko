package dziecko.models.dtos;

import java.io.Serializable;
import java.util.List;

import dziecko.models.Area;

public class AreaList implements Serializable {
    private static final long serialVersionUID = 8337373684273328140L;

    private List<Area> areas;

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    AreaList() {
    }
}
