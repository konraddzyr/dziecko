package dziecko.models.dtos;

public class ChildValidationDto {
	private boolean outOfAreas;
	private boolean confirmationRequired;

	public ChildValidationDto(boolean outOfAreas, boolean confirmationRequired) {
		this.outOfAreas = outOfAreas;
		this.confirmationRequired = confirmationRequired;
	}

	public boolean isOutOfAreas() {
		return outOfAreas;
	}

	public boolean isConfirmationRequired() {
		return confirmationRequired;
	}
	
}
