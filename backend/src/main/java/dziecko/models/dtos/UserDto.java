package dziecko.models.dtos;

import dziecko.models.User;

public class UserDto extends AbstractDto implements EntityGenerator<User> {

    private static final long serialVersionUID = 6454966207764168519L;

    public UserDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public User createEntity() {
        return new User();
    }

}
