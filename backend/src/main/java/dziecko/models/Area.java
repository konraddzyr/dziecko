package dziecko.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import dziecko.models.dtos.AreaDto;

@Entity
@Table(name = "AREAS")
// TODO: hql
@NamedNativeQueries({
                     @NamedNativeQuery(name = "findNowAreasForId", query = "SELECT a.id,a.name,a.latitude,a.longitude,a.radius FROM AREADATES ad"
                             + " INNER JOIN AREAS_AREADATES aad on ad.id=aad.areadate_id"
                             + " INNER JOIN AREAS a ON aad.area_id=a.id WHERE ad.start_time<=now() AND now()<=ad.end_time"
                             + " AND a.child_id=?", resultClass = Area.class),
                     @NamedNativeQuery(name = "findAreasForIdAndDate", query = "SELECT a.id,a.name,a.latitude,a.longitude,a.radius FROM AREADATES ad"
                             + " INNER JOIN AREAS_AREADATES aad on ad.id=aad.areadate_id"
                             + " INNER JOIN AREAS a ON aad.area_id=a.id WHERE ad.start_time<=?1 AND ?1<=ad.end_time"
                             + " AND a.child_id=?2", resultClass = Area.class) })
public class Area extends AbstractBase implements DtoGenerator<AreaDto> {

    private static final long serialVersionUID = -1179388547197710459L;

    @Column(name = "LATITUDE")
    private Double latitude;

    @Column(name = "LONGITUDE")
    private Double longitude;

    // should be in km
    @Column(name = "RADIUS")
    private Double radius;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(name = "AREAS_AREADATES", joinColumns = {@JoinColumn(name = "AREA_ID", nullable = false, updatable = false) }, inverseJoinColumns = {@JoinColumn(name = "AREADATE_ID", nullable = false, updatable = false) })
    private List<AreaDate> areaDates = new ArrayList<>();

    public List<AreaDate> getAreaDates() {
        return areaDates;
    }

    public Area() {
    }

    public Area(Integer id, String name, Double latitude, Double longitude, Double radius) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getRadius() {
        return radius;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        Area a = (Area) obj;
        return new EqualsBuilder().append(id, a.id).isEquals();
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return new HashCodeBuilder(3, 41).append(id).toHashCode();
        } else {
            return new HashCodeBuilder(3, 41).append(latitude).append(longitude).toHashCode();
        }
    }

    @Override
    public AreaDto createDto() {
        return new AreaDto(id, name, latitude, longitude, radius, null);
    }

    public AreaDto createDtoWithDate(AreaDate date) {
        return new AreaDto(id, name, latitude, longitude, radius, date);
    }

}
