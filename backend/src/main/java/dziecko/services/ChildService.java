package dziecko.services;

import java.util.List;
import java.util.Set;

import javax.jws.WebService;

import dziecko.models.Area;
import dziecko.models.Child;
import dziecko.models.dtos.ChildValidationDto;

@WebService
public interface ChildService {

    public boolean validateCircle(Integer id, double y, double x);

    public boolean validateCircleSql(Integer id, double y, double x);

    public boolean validateCircleMix(Integer id, double y, double x);

    // TODO: dto
    public Child getChild(Integer id);

    public Set<Area> getAreasForChildAndData(Integer id, String date);

    public Set<Area> setAreasForChildAndData(Integer id, String date, List<Area> areas);

    public boolean processValidation(String token, double y, double x, int option);
    
    public ChildValidationDto processValidationForMobile(String token, double y, double x, int option);
    
    public void confirmation(String token, boolean value);

}
