package dziecko.services;

import javax.jws.WebMethod;
import javax.jws.WebService;

import dziecko.models.Parent;

@WebService
public interface AuthenticationService {

    // TODO: dto
    @WebMethod
    public Parent authenticate(String username, String password);

    @WebMethod
    public Integer authenticateMobile(String name, String pass);

    @WebMethod
    public void deauthenticate(String username);

}
