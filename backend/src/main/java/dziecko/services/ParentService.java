package dziecko.services;

import java.util.List;
import java.util.Set;

import javax.jws.WebService;

import dziecko.models.Child;
import dziecko.models.Parent;
import dziecko.models.dtos.PairChildDto;
import dziecko.models.dtos.ValidatedChildDto;

@WebService
public interface ParentService {

    // TODO: dto
    public Parent createParent(Parent parent);

    // TODO dto
    public Set<Child> readChildrenForParent(Integer parentId);

    // TODO dto, maybe child creation should be inside childService?
    public Parent createChildrenForParent(Integer parentId, Child child);

    // TODO dto, maybe child update should be inside childService?
    public Parent updateChildrenForParent(Integer parentId, Child child);

    public Boolean checkIfNotExists(String name);

    public PairChildDto getChildToken(String parentName, String password, String childName);

    public List<ValidatedChildDto> validateAllChildren(Integer id);

    public void requestConfirmation(Integer parentId, String childName);
}
