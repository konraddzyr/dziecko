package dziecko.services.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import dziecko.models.Child;
import dziecko.models.Parent;
import dziecko.repositories.ChildRepository;
import dziecko.repositories.ParentRepository;
import dziecko.services.AuthenticationService;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    @Autowired
    private ParentRepository parentRepository;

    @Autowired
    private ChildRepository childRepository;

    @Override
    public Parent authenticate(String username, String password) throws BadCredentialsException {
        Authentication authentication = this.authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return parentRepository.findByName(username);

        // TODO: services should be secured (authentication)
    }

    @Override
    public Integer authenticateMobile(String name, String pass) throws BadCredentialsException {
        Authentication authentication = this.authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(name, pass));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Parent par = parentRepository.findByName(name);
        return par == null ? -1 : par.getId();

        // TODO: services should be secured (authentication)
    }

    @Override
    public void deauthenticate(String username) {
        Set<Child> kids = parentRepository.findByName(username).getChildren();
        for (Child kid : kids) {
            if (!kid.getAlarm()) {
                kid.setIsImportant(false);
                childRepository.save(kid);
            }
        }
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}
