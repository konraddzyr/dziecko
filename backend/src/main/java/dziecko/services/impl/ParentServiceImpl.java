package dziecko.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dziecko.models.Child;
import dziecko.models.Parent;
import dziecko.models.dtos.PairChildDto;
import dziecko.models.dtos.ValidatedChildDto;
import dziecko.repositories.ParentRepository;
import dziecko.services.ParentService;

@Component
// (value = "parentService")
public class ParentServiceImpl implements ParentService {

    @Autowired
    ParentRepository parentRepository;

    @Override
    @Transactional
    public Parent createParent(Parent parent) {
        if (!checkIfNotExists(parent.getName())) {
            return parentRepository.save(parent);
        }

        return null;
    }

    @Override
    // @PreAuthorize("hasPermission(#parentId, 'eq')")
    public Set<Child> readChildrenForParent(Integer parentId) {

        return parentRepository.findOne(parentId).getChildren();

        /*
         * Set<Child> children = parentRepository.findOne(parentId).getChildren(); Set<ChildDto> resultSet = new
         * HashSet<>(); Iterator<Child> it = children.iterator(); while (it.hasNext()) {
         * resultSet.add(it.next().createDto()); }
         * 
         * return resultSet;
         */

    }

    @Override
    @Transactional
    // @PreAuthorize("hasPermission(#parentId, 'eq')")
    public Parent createChildrenForParent(Integer parentId, Child child) {
        Parent parent = parentRepository.findOne(parentId);
        parent.addChild(child);
        return parentRepository.save(parent);
    }

    @Override
    @Transactional
    // @PreAuthorize("hasPermission(#parentId, 'eq')")
    public Parent updateChildrenForParent(Integer parentId, Child child) {
        Parent parent = parentRepository.findOne(parentId);
        parent.updateChild(child);
        return parentRepository.save(parent);
    }

    private Child getLastElement(final Collection<Child> c) {
        final Iterator<Child> itr = c.iterator();
        Object lastElement = itr.next();
        while (itr.hasNext()) {
            lastElement = itr.next();
        }
        return (Child) lastElement;
    }

    @Override
    public PairChildDto getChildToken(String parentName, String password, String childName) {
        Parent parent = parentRepository.findByName(parentName);
        if (null == parent) {
            return new PairChildDto(false, "badParentName");
        }
        if (!password.equals(parent.getPassword())) {
            return new PairChildDto(false, "badPassword");
        }
        for (Child child : parent.getChildren()) {
            if (child.getName().equals(childName)) {
                return new PairChildDto(false, child.getToken());
            }
        }
        parent = this.createChildrenForParent(parent.getId(), new Child(childName));

        Child kid = this.getLastElement(parent.getChildren());
        return new PairChildDto(true, kid.getToken());
    }

    @Override
    public Boolean checkIfNotExists(String name) {
        return null == parentRepository.findByName(name);
    }

    @Override
    public List<ValidatedChildDto> validateAllChildren(Integer id) {
    	List<ValidatedChildDto> result = new ArrayList<ValidatedChildDto>();
    	ValidatedChildDto element;
    	
        Parent par = parentRepository.findOne(id);
        if (par != null) {
            for (Child kid : par.getChildren()) {
                element = new ValidatedChildDto(kid.getName(), kid.getAlarm(), kid.getChildResponsed(), kid.getResponse());
                result.add(element);
                if(kid.getChildResponsed()) {
                	kid.setChildResponsed(false);
                	kid.setResponse(false);
                }
            }
        }
        parentRepository.save(par);
        return result;
    }

    @Override
    @Transactional
    public void requestConfirmation(Integer parentId, String childName) {
        Parent par = parentRepository.findOne(parentId);
        if (par != null) {
            for (Child kid : par.getChildren()) {
                if (kid.getName() == childName) {
                    kid.setConfirmationRequired(true);
                }
            }
        }
        parentRepository.save(par);
    }

}
