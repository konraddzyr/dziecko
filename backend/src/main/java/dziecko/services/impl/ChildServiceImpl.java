package dziecko.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dziecko.models.Area;
import dziecko.models.AreaDate;
import dziecko.models.Child;
import dziecko.repositories.AreaRepository;
import dziecko.models.dtos.ChildValidationDto;
import dziecko.repositories.ChildRepository;
import dziecko.services.ChildService;

// x is longitude, y is latitude
@Component
// (value = "childService")
public class ChildServiceImpl implements ChildService {

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private EntityManagerFactory emf;

    @Override
    public boolean validateCircle(Integer id, double y, double x) {
        Child kid = childRepository.findOne(id);
        Set<Area> areas = kid.getAreas();

        Date d = new Date();
        double y0 = deg2rad(y);
        double x0 = deg2rad(x);

        if (!areas.isEmpty()) {
            for (Area a : areas) {
                for (AreaDate ad : a.getAreaDates()) {
                    if (ad.getStartTime().before(d) && ad.getEndTime().after(d)) {

                        // Haversine Formula to calculate a distance between 2
                        // points determined by lat- and longitude

                        double dlat = y0 - deg2rad(a.getLatitude());
                        double dlon = x0 - deg2rad(a.getLongitude());

                        double aSin1 = Math.sin(dlat / 2);
                        double aSin2 = Math.sin(dlon / 2);

                        double eqA = aSin1 * aSin1 + Math.cos(y0) * Math.cos(deg2rad(a.getLatitude())) * aSin2 * aSin2;
                        double eqC = 2 * Math.atan2(Math.sqrt(eqA), Math.sqrt(1 - eqA));

                        double distanceInKm = eqC * ChildRepository.EARTH_RADIUS;

                        // distance should be smaller than radius+tolerance
                        if (distanceInKm <= a.getRadius() + ChildRepository.TOLERANCE) {
                            // if there is a circle with x and y then break
                            return true;
                        }
                    }
                }
            }
        } else {
            return true;
        }

        return false;
    }

    // convert degrees to radians
    private double deg2rad(double deg) {
        return deg * Math.PI / 180;
    }

    // probably the fastest option in our case
    @Override
    public boolean validateCircleSql(Integer id, double y, double x) {
        return childRepository.countAreas(id, y, x) > 0;
    }

    @Override
    public boolean validateCircleMix(Integer id, double y, double x) {

        double y0 = deg2rad(y);
        double x0 = deg2rad(x);

        TypedQuery<Area> query = emf.createEntityManager().createNamedQuery("findNowAreasForId", Area.class)
                .setParameter(1, id);
        List<Area> areasToCheck = query.getResultList();

        if (!areasToCheck.isEmpty()) {
            for (Area a : areasToCheck) {

                // Haversine Formula to calculate a distance between 2 points
                // determined by lat- and longitude

                double dlat = y0 - deg2rad(a.getLatitude());
                double dlon = x0 - deg2rad(a.getLongitude());

                double aSin1 = Math.sin(dlat / 2);
                double aSin2 = Math.sin(dlon / 2);

                double eqA = aSin1 * aSin1 + Math.cos(y0) * Math.cos(deg2rad(a.getLatitude())) * aSin2 * aSin2;
                double eqC = 2 * Math.atan2(Math.sqrt(eqA), Math.sqrt(1 - eqA));

                double distanceInKm = eqC * ChildRepository.EARTH_RADIUS;

                // distance should be smaller than radius+tolerance
                if (distanceInKm <= a.getRadius() + ChildRepository.TOLERANCE) {
                    // if there is a circle with x and y then break
                    return true;
                }
            }
        } else {
            return true;
        }

        return false;
    }

    @Override
    @Transactional
    public Child getChild(Integer id) {
        Child kid = childRepository.findOne(id);
        kid.setIsImportant(true);
        return childRepository.save(kid);
    }

    @Override
    public Set<Area> getAreasForChildAndData(Integer id, String date) {

        Child kid = childRepository.findOne(id);
        Set<Area> areas = new HashSet<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (Area a : kid.getAreas()) {
            for (AreaDate ad : a.getAreaDates()) {
                if (sdf.format(ad.getStartTime()).equals(date)) {
                    areas.add(a);
                    break;
                }
            }
        }

        return areas;
    }

    @Override
    public boolean processValidation(String token, double y, double x, int option) {
        Child kid = this.childRepository.findByToken(token);
        
        return this.validateChild(y, x, option, kid);
    }
    
    @Override
    public ChildValidationDto processValidationForMobile(String token, double y, double x, int option) {
        Child kid = this.childRepository.findByToken(token);
        
        return new ChildValidationDto(!this.validateChild(y, x, option, kid), kid.getConfirmationRequired());
    }
    
    private boolean validateChild(double y, double x, int option, Child kid) {
    	boolean result = false;
    	if (null != kid) {
            if (kid.getIsImportant()) {
                // we need to update dimensions each time
                kid.setLatitude(y);
                kid.setLongitude(x);

                kid = childRepository.save(kid);
            }

            switch (option) {
            case 1:
                result = validateCircle(kid.getId(), y, x);
                break;
            case 2:
                result = validateCircleSql(kid.getId(), y, x);
                break;
            default:
                result = validateCircleMix(kid.getId(), y, x);
            }

            if (result && kid.getAlarm()) {
                kid.setAlarm(false);
                childRepository.save(kid);
            } else if (!result && !kid.getAlarm()) {
                // if result is still false, so child is out of circles
                // only if we need to change we're changing status
                kid.setAlarm(true);
                childRepository.save(kid);

                Properties properties = System.getProperties();
                properties.setProperty("mail.smtp.host", "localhost");
                Session session = Session.getDefaultInstance(properties);
                try {
                    MimeMessage message = new MimeMessage(session);

                    message.setFrom(new InternetAddress("bezpieczne@dziecko.pl"));
                    /*
                     * for (Parent par : kid.getParents()) { message.addRecipient(Message.RecipientType.TO, new
                     * InternetAddress(par.getMail())); }
                     */

                    message.setSubject("This is the Subject Line!");
                    message.setText("This is actual message");

                    // Transport.send(message);
                    System.out.println("Sent message successfully...");
                } catch (MessagingException mex) {
                    mex.printStackTrace();
                }
            }
        }
		return result;
    }

    @Override
    public Set<Area> setAreasForChildAndData(Integer id, String date, List<Area> areas) {
        Child kid = childRepository.findOne(id);
        kid.removeAreas(getAreasForChildAndData(id, date));
        kid.addAreas(areas);
        childRepository.save(kid);
        return getAreasForChildAndData(id, date);
    }
    
    @Override
    @Transactional
    public void confirmation(String token, boolean value) {
    	Child kid = this.childRepository.findByToken(token);
    	kid.setConfirmationRequired(false);
    	kid.setChildResponsed(true);
    	kid.setResponse(value);
    	this.childRepository.save(kid);
    }
}
