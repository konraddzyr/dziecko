package dziecko.repositories;

import dziecko.models.Parent;
import dziecko.utils.BaseCrudRepository;

public interface ParentRepository extends BaseCrudRepository<Parent, Integer> {

    public Integer findIdByName(String name);

    public Parent findByName(String name);
}
