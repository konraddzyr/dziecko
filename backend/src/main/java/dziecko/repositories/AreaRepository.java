package dziecko.repositories;

import dziecko.models.Area;
import dziecko.utils.BaseCrudRepository;

public interface AreaRepository extends BaseCrudRepository<Area, Integer> {

}
