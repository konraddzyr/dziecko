package dziecko.repositories;

import dziecko.models.AreaDate;
import dziecko.utils.BaseCrudRepository;

public interface AreaDateRepository extends BaseCrudRepository<AreaDate, Integer> {

}
