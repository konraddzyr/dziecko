package dziecko.repositories;

import dziecko.models.News;
import dziecko.utils.BaseCrudRepository;

public interface NewsRepository extends BaseCrudRepository<News, Integer> {

}
