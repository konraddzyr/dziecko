package dziecko.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import dziecko.models.Child;
import dziecko.utils.BaseCrudRepository;

public interface ChildRepository extends BaseCrudRepository<Child, Integer> {

    public static final double TOLERANCE = 0.01; // km
    public static final double EARTH_RADIUS = 6371; // km

    // warning - native sql determines db independence lost

    // Haversine Formula inside SQL
    public final static String FIND_NUMBER_OF_AREAS_INSIDE = "SELECT count(a.id) FROM AREADATES ad"
            + " INNER JOIN AREAS_AREADATES aad on ad.id=aad.areadate_id"
            + " INNER JOIN AREAS a ON aad.area_id=a.id WHERE ad.start_time<=now() AND now()<=ad.end_time"
            + " AND a.child_id=:id"
            + " AND 2*ATAN2(SQRT(SIN((:y*PI()/180-a.latitude*PI()/180)/2)*SIN((:y*PI()/180-a.latitude*PI()/180)/2)"
            + "+COS(:y*PI()/180)*COS(a.latitude*PI()/180)"
            + "*SIN((:x*PI()/180-a.longitude*PI()/180)/2)*SIN((:x*PI()/180-a.longitude*PI()/180)/2)),"
            + "SQRT(1-SIN((:y*PI()/180-a.latitude*PI()/180)/2)*SIN((:y*PI()/180-a.latitude*PI()/180)/2)"
            + "+COS(:y*PI()/180)*COS(a.latitude*PI()/180)"
            + "*SIN((:x*PI()/180-a.longitude*PI()/180)/2)*SIN((:x*PI()/180-a.longitude*PI()/180)/2)))*" + EARTH_RADIUS
            + "<=a.radius+" + TOLERANCE;

    @Query(value = FIND_NUMBER_OF_AREAS_INSIDE, nativeQuery = true)
    public int countAreas(@Param("id") Integer id, @Param("y") double y, @Param("x") double x);

    public Child findByToken(String token);

    public Integer findIdByName(String name);
}
