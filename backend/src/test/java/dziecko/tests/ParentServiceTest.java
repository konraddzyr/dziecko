package dziecko.tests;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import dziecko.models.Child;
import dziecko.models.Parent;
import dziecko.repositories.ParentRepository;
import dziecko.services.ParentService;
import dziecko.tests.utils.BaseForTests;
import dziecko.utils.builders.ChildBuilder;
import dziecko.utils.builders.ParentBuilder;

public class ParentServiceTest extends BaseForTests {

    @Autowired
    private ParentRepository parentRepository;

    @Autowired
    private ParentService parentService;

    @Test
    public void shouldCreateChild() {
        // given
        Child kid = new ChildBuilder().withLatitiude(51.26705725207446).withLongtitiude(22.52069592475891)
                .withName("Agatka").build();
        Parent parent = parentRepository.save(new ParentBuilder().withRelationToChild(kid).build());

        int totalChildren = parent.getChildren().size();

        // when
        parent = parentService.createChildrenForParent(parent.getId(),
                new ChildBuilder().withLatitiude(51.26705725207446).withLongtitiude(22.52069592475891)
                        .withName("Kamil").build());

        // then
        Assert.assertEquals(parent.getChildren().size(), totalChildren + 1);
        Assert.assertNotNull(parent.getChildren().toArray(new Child[parent.getChildren().size()])[totalChildren]
                .getId());
    }

    @Test
    public void shouldUpdateChild() {
        String first = "Agatka";
        String second = "Agusia";

        // given
        Child kid = new ChildBuilder().withLatitiude(51.26705725207446).withLongtitiude(22.52069592475891)
                .withName(first).build();
        Parent parent = parentRepository.save(new ParentBuilder().withRelationToChild(kid).build());

        kid = parent.getChildren().toArray(new Child[parent.getChildren().size()])[0];
        int totalChildren = parent.getChildren().size();

        // when
        kid.setName(second);
        parent = parentService.updateChildrenForParent(parent.getId(), kid);
        Child kidAfter = parent.getChildren().toArray(new Child[parent.getChildren().size()])[0];

        // then
        Assert.assertEquals(parent.getChildren().size(), totalChildren);
        Assert.assertNotEquals(kidAfter.getName(), first);
        Assert.assertEquals(kidAfter.getName(), second);
    }
}
