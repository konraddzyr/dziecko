package dziecko.tests;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import dziecko.repositories.AreaDateRepository;
import dziecko.repositories.ParentRepository;
import dziecko.services.ParentService;
import dziecko.tests.utils.BaseForTests;

public class SpringConfigurationTest extends BaseForTests {

    @Autowired
    private ParentRepository parRepo;

    @Autowired
    private ParentService parentService;

    @Autowired
    private AreaDateRepository adRepo;

    @Autowired
    private EntityManagerFactory emf;

    @Test
    public void shouldWireComponents() {
        Assert.assertNotNull(parRepo);
        Assert.assertNotNull(parentService);
        Assert.assertNotNull(adRepo);
        Assert.assertNotNull(emf);
    }
}
