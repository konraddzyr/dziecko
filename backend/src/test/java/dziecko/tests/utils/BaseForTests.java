package dziecko.tests.utils;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@ContextConfiguration("classpath:META-INF/test-context.xml")
public class BaseForTests extends AbstractTestNGSpringContextTests {

    @SuppressWarnings("resource")
    public BaseForTests() {
        new AnnotationConfigApplicationContext().getEnvironment().setActiveProfiles("test");
    }

}
