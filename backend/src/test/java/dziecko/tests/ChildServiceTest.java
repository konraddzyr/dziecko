package dziecko.tests;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import dziecko.models.Area;
import dziecko.models.AreaDate;
import dziecko.models.Child;
import dziecko.repositories.AreaDateRepository;
import dziecko.repositories.AreaRepository;
import dziecko.repositories.ChildRepository;
import dziecko.services.ChildService;
import dziecko.tests.utils.BaseForTests;
import dziecko.utils.builders.AreaBuilder;
import dziecko.utils.builders.AreaDateBuilder;
import dziecko.utils.builders.ChildBuilder;

public class ChildServiceTest extends BaseForTests {

    @Autowired
    private AreaDateRepository areaDateRepository;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private ChildService childService;

    // given values
    private static final long HOUR = 3600 * 1000; // in milli-seconds.
    private Date date = new Date();

    private AreaDate betweenNowAndNowPlus2h;

    private AreaBuilder tarasowaBuilder;

    @AfterMethod
    private void clearRepository() {
        areaRepository.deleteAll();
        areaDateRepository.deleteAll();
        childRepository.deleteAll();
    }

    @BeforeMethod
    private void prepareData() {
        betweenNowAndNowPlus2h = new AreaDateBuilder().withStartTime(date)
                .withEndTime(new Date(date.getTime() + 2 * HOUR)).build();

        // Tarasowa 4, Lublin, Poland
        tarasowaBuilder = new AreaBuilder().withLatitiude(51.26705725207446).withLongtitiude(22.52069592475891);
    }

    @Test
    public void serviceChildInsideOnlyCircle() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.15).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertEquals(childRepository.count(), 1); // checking if adding to repository works
        Assert.assertTrue(childService.validateCircle(child.getId(), x, y));
    }

    @Test
    public void sqlChildInsideOnlyCircle() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.15).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertEquals(childRepository.count(), 1); // the same assertion as above, to ensure tests're doing the
                                                         // same
        Assert.assertTrue(childService.validateCircleSql(child.getId(), x, y));
    }

    @Test
    public void mixChildInsideOnlyCircle() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.15).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertEquals(childRepository.count(), 1); // the same assertion as above, to ensure tests're doing the
                                                         // same
        Assert.assertTrue(childService.validateCircleMix(child.getId(), x, y));
    }

    @Test
    public void serviceChildInsideOnlyCircleTolerance() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.14).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        // for this points distance is about 145 meters, so radius 140 is too low
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertTrue(childService.validateCircle(child.getId(), x, y));
    }

    @Test
    public void sqlChildInsideOnlyCircleTolerance() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.14).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertTrue(childService.validateCircleSql(child.getId(), x, y));
    }

    @Test
    public void mixChildInsideOnlyCircleTolerance() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.14).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertTrue(childService.validateCircleMix(child.getId(), x, y));
    }

    @Test
    public void serviceChildOutsideOnlyCircle() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.13).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        // for this points distance is about 145 meters
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertFalse(childService.validateCircle(child.getId(), x, y));
    }

    @Test
    public void sqlChildOutsideOnlyCircle() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.13).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertFalse(childService.validateCircleSql(child.getId(), x, y));
    }

    @Test
    public void mixChildOutsideOnlyCircle() {
        // given
        Area area = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.13).build(); // radius in km
        Child child = childRepository.save(new ChildBuilder().withRelationToArea(area).build());

        // when
        double x = 51.26576;
        double y = 22.520878;

        // then
        Assert.assertFalse(childService.validateCircleMix(child.getId(), x, y));
    }

    @Test(groups = {"ManyCircles" })
    public void serviceChildInsideSeventhCircle() {
        // given
        Child child = saveChildWithManyAreas();

        // when
        double x = 51.26167315960279;
        double y = 22.52894639968872;

        // then
        Assert.assertTrue(childService.validateCircle(child.getId(), x, y));
    }

    @Test(groups = {"ManyCircles" })
    public void sqlChildInsideSeventhCircle() {
        // given
        Child child = saveChildWithManyAreas();

        // when
        double x = 51.26167315960279;
        double y = 22.52894639968872;

        // then
        Assert.assertTrue(childService.validateCircleSql(child.getId(), x, y));
    }

    @Test(groups = {"ManyCircles" })
    public void mixChildInsideSeventhCircle() {
        // given
        Child child = saveChildWithManyAreas();

        // when
        double x = 51.26167315960279;
        double y = 22.52894639968872;

        // then
        Assert.assertTrue(childService.validateCircleMix(child.getId(), x, y));
    }

    @Test(groups = {"ManyCircles" })
    public void serviceChildOutsideCircles() {
        // given
        Child child = saveChildWithManyAreas();

        // when
        double x = 51.26167315960279;
        double y = 25.52894639968872;

        // then
        Assert.assertFalse(childService.validateCircle(child.getId(), x, y));
    }

    @Test
    public void sqlChildOutsideCircles() {
        // given
        Child child = saveChildWithManyAreas();

        // when
        double x = 51.26167315960279;
        double y = 25.52894639968872;

        // then
        Assert.assertFalse(childService.validateCircleSql(child.getId(), x, y));
    }

    @Test
    public void mixChildOutsideCircles() {
        // given
        Child child = saveChildWithManyAreas();

        // when
        double x = 51.26167315960279;
        double y = 25.52894639968872;

        // then
        Assert.assertFalse(childService.validateCircleMix(child.getId(), x, y));
    }

    public void serviceChildNoCircles() {
        // given
        Child child = childRepository.save(new ChildBuilder().build());

        // when
        double x = 51.26167315960279;
        double y = 25.52894639968872;

        // then
        Assert.assertTrue(childService.validateCircle(child.getId(), x, y));
    }

    @Test
    public void sqlChildNoCircles() {
        // given
        Child child = childRepository.save(new ChildBuilder().build());

        // when
        double x = 51.26167315960279;
        double y = 25.52894639968872;

        // then
        // IN THE CASE OF SQL QUERY CHILD WITH NO AREAS AREN'T VALID
        Assert.assertFalse(childService.validateCircleSql(child.getId(), x, y));
    }

    @Test
    public void mixChildNoCircles() {
        // given
        Child child = childRepository.save(new ChildBuilder().build());

        // when
        double x = 51.26167315960279;
        double y = 25.52894639968872;

        // then
        Assert.assertTrue(childService.validateCircleMix(child.getId(), x, y));
    }

    // TODO: test dla z�ej godziny - minut� przed zadan� i ma wywali�
    // i drugi test, �e do p�nocy jednego dnia i od 00:01 dnia nast�pnego

    @Test
    public void get11AreasForChild() {
        // given
        Child child = saveChildWithManyAreas();

        // when + then
        Assert.assertEquals(
                childService.getAreasForChildAndData(child.getId(),
                        new SimpleDateFormat("yyyy-MM-dd").format(new Date())).size(), 11);
    }

    private Child saveChildWithManyAreas() {
        // road from Tarasowa to G�rki Czechowskie
        Area area1 = tarasowaBuilder.withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build(); // radius in
                                                                                                         // km
        Area area2 = new AreaBuilder().withLatitiude(51.2658891858365).withLongtitiude(22.520095109939575)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area3 = new AreaBuilder().withLatitiude(51.26481507573857).withLongtitiude(22.52071738243103)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area4 = new AreaBuilder().withLatitiude(51.264184024349866).withLongtitiude(22.52318501472473)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area5 = new AreaBuilder().withLatitiude(51.26367380625418).withLongtitiude(22.52543807029724)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area6 = new AreaBuilder().withLatitiude(51.26297560389227).withLongtitiude(22.527412176132202)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area7 = new AreaBuilder().withLatitiude(51.26211626334849).withLongtitiude(22.529386281967163)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area8 = new AreaBuilder().withLatitiude(51.26104206507564).withLongtitiude(22.530630826950073)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area9 = new AreaBuilder().withLatitiude(51.259658997832254).withLongtitiude(22.531660795211792)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area10 = new AreaBuilder().withLatitiude(51.258839880135795).withLongtitiude(22.53344178199768)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.1).build();
        Area area11 = new AreaBuilder().withLatitiude(51.25834303114282).withLongtitiude(22.535158395767212)
                .withRelationToDate(betweenNowAndNowPlus2h).withRadius(0.15).build();

        return childRepository.save(new ChildBuilder().withRelationToArea(area1).withRelationToArea(area2)
                .withRelationToArea(area3).withRelationToArea(area4).withRelationToArea(area5)
                .withRelationToArea(area6).withRelationToArea(area7).withRelationToArea(area8)
                .withRelationToArea(area9).withRelationToArea(area10).withRelationToArea(area11).build());
    }
}
